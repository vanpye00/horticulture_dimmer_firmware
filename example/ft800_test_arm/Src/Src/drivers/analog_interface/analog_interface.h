#ifndef ANALOG_INTERFACE_H
#define ANALOG_INTERFACE_H

#define ADC_CONVERTED_DATA_BUFFER_SIZE   ((uint32_t)  32)   /* Size of array aADCxConvertedData[] */

#include "stm32f7xx_hal.h"

typedef enum
{
    ANALOG_CHANNEL_ZERO_CROSS,
    ANALOG_CHANNEL_CH1_CONTROL,
    ANALOG_CHANNEL_CH2_CONTROL,
    ANALOG_CHANNEL_CH3_CONTROL,
    ANALOG_CHANNEL_CH4_CONTROL,
    MAX_NUM_ANALOG_CHANNEL
} enum_analog_channel_name;

ADC_HandleTypeDef             AdcHandle;
ADC_ChannelConfTypeDef        sConfig;

#endif