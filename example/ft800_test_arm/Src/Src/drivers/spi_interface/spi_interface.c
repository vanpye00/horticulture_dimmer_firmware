/**
  ******************************************************************************
  * C Library for FT800 EVE module 
  ******************************************************************************
  * @author  Akos Pasztor    (http://akospasztor.com)
  * @file    spi.c
  * @brief   SPI functions
  *          This file contains the GPIO initializations and functions
  *          for SPI peripherials.
  ******************************************************************************
  * Copyright (c) 2014 Akos Pasztor. All rights reserved.
  ******************************************************************************
**/

#include "stm32f3xx.h"
#include "main.h"
#include "spi_interface.h"

#define PIN_EVE_CS      PIN_SPI1_CS_GPIO_Port, PIN_SPI1_CS_Pin
#define PIN_EVE_PD      PIN_LCD_PD_GPIO_Port, PIN_LCD_PD_Pin
#define SPI_TIME        1000
void EVE_cs_set(void)
{
  HAL_GPIO_WritePin(PIN_EVE_CS, GPIO_PIN_SET);
}

void EVE_cs_clear(void)
{
  HAL_GPIO_WritePin(PIN_EVE_CS, GPIO_PIN_RESET);
}

void EVE_pdn_set(void)
{
  HAL_GPIO_WritePin(PIN_EVE_PD, GPIO_PIN_SET);
}

void EVE_pdn_clear(void)
{
  HAL_GPIO_WritePin(PIN_EVE_PD, GPIO_PIN_RESET);
}

void spi_transmit_async(uint8_t data)
{
  HAL_SPI_Transmit(&hspi1, &data, 1, SPI_TIME);
}
void spi_transmit(uint8_t data)
{
  HAL_SPI_Transmit(&hspi1, &data, 1, SPI_TIME);
 
}
uint8_t spi_receive(uint8_t pTxData)
{
  uint8_t pRxData;
  HAL_SPI_TransmitReceive(&hspi1, &pTxData, &pRxData, 1, SPI_TIME);  
  return pRxData;
}
uint8_t fetch_flash_byte(const uint8_t *data)
{
  return *data;
}