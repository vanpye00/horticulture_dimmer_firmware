#ifndef SPI_INTERFACE_H
#define SPI_INTERFACE_H

void EVE_pdn_set(void);
void EVE_pdn_clear(void);
void EVE_cs_set(void);
void EVE_cs_clear(void);
void spi_transmit_async(uint8_t data);
void spi_transmit(uint8_t data);
uint8_t spi_receive(uint8_t data);
uint8_t fetch_flash_byte(const uint8_t *data);

#endif