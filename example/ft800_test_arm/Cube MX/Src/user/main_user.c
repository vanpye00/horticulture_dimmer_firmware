#include "main_user.h"
#include "ft800.h"
#include "spi_interface.h"
#include "gpio_interface.h"
#include "ui_interface.h"
#include "timer_interface.h"
#include "adc_interface.h"

void lcd_start_screen(uint8_t button);
uint32_t tag;
uint32_t tag_id;
uint16_t zcd_point;

int main_user(void)
{ 
	while(initFT800());
	delay_ms(50);
  
	ui_clear_screen();
  ui_draw_splash_screen();
  Timer_Start();
  ADC_Start();
  zcd_point = ADC_Get_ZCD();
  ui_set_calibrate_parameter();
  ui_draw_main_menu();

	while(1)
	{

		tag = ui_get_touched_tag_id();
		//button pressed
		if(tag != 0)
		{
      ui_process_new_button_touched(tag);
      delay_ms(50);
      tag = ui_get_touched_tag_id();
      while (tag!=0)
      {
        tag = ui_get_touched_tag_id(); 
      }      
 		}
    if(ui_is_not_respond())
    {
      initFT800();
      delay_ms(50);
      ui_set_calibrate_parameter();
      ui_draw_current_screen();
    }
    
    
	}    
    return 0;
}



void lcd_start_screen(uint8_t button)
{
	cmd(CMD_DLSTART);
	cmd(CLEAR_COLOR_RGB(0,0,0));
	cmd(CLEAR(1,1,1));
	//cmd_text(10,245, 27,0, "Designed by: Akos Pasztor");
    
	//cmd_gradient(0,0,0xA1E1FF, 0,250,0x000080);
	//cmd_text(10,245, 27,0, "Designed by: Akos Pasztor");
	//cmd_text(470,250, 26,OPT_RIGHTX, "http://akospasztor.com");
	//cmd(COLOR_RGB(0xDE,0x00,0x08));
	//cmd_text(240,40, 31,OPT_CENTERX, "FT800 Demo");
	cmd(COLOR_RGB(255,255,255));
	cmd(TAG(1));

	if(!button)
	{
		cmd_fgcolor(0x228B22);
		cmd_button(130,150, 220,48, 28,0, "Tap to Continue");
	}
	else
	{
		cmd_fgcolor(0x0A520A);
		cmd_button(130,150, 220,48, 28,OPT_FLAT, "Tap to Continue");
	}

	cmd(DISPLAY());
	cmd(CMD_SWAP);	
}
