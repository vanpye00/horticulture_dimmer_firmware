#ifndef MAIN_USER_H
#define MAIN_USER_H

#include "stm32f3xx_hal.h"

extern ADC_HandleTypeDef hadc2;
extern DMA_HandleTypeDef hdma_adc2;

extern I2C_HandleTypeDef hi2c1;

extern SPI_HandleTypeDef hspi1;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;

extern PCD_HandleTypeDef hpcd_USB_FS;

int main_user(void);
#endif