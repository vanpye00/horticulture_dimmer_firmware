#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#define DISPLAY_HEIGHT (uint32_t)272
#define DISPLAY_WIDTH (uint32_t)480
#define DISPLAY_CENTER_X (DISPLAY_WIDTH/2)
#define DISPLAY_CENTER_Y (DISPLAY_HEIGHT/2)

//Color Commands
#define GET_RGB_R(__COLOR__) (__COLOR__>>16)
#define GET_RGB_G(__COLOR__) ((__COLOR__>>8)&0xff)
#define GET_RGB_B(__COLOR__) (__COLOR__&0xff)
#define FT_CLEAR_RGB(__COLOR__) cmd(CLEAR_COLOR_RGB(GET_RGB_R(__COLOR__),GET_RGB_G(__COLOR__),GET_RGB_B(__COLOR__)))
#define FT_CLEAR(__COLOR__, __STENCIL__, __TAG__) cmd(CLEAR(__COLOR__, __STENCIL__, __TAG__))
#define FT_CLEAR_BUFFER() FT_CLEAR(CLEAR_COLOR_BUFFER, CLEAR_STENCIL_BUFFER, CLEAR_TAG_BUFFER)
#define FT_SET_COLOR(__COLOR__) cmd(COLOR_RGB(GET_RGB_R(__COLOR__),GET_RGB_G(__COLOR__),GET_RGB_B(__COLOR__)))
#define FT_FGCOLOR(__COLOR__) cmd_fgcolor(__COLOR__)

//Clear buffer parameters
#define CLEAR_COLOR_BUFFER 1
#define CLEAR_STENCIL_BUFFER 1
#define CLEAR_TAG_BUFFER 1
#define KEEP_COLOR_BUFFER 1
#define KEEP_STENCIL_BUFFER 1
#define KEEP_TAG_BUFFER 1

#define DEFAULT_BACKGROUND COLOR_WHITE
#define COLOR_WHITE 0xffffff
#define COLOR_BLACK 0
#define COLOR_GRAY 0xD3D3D3

typedef enum
{
  SCREEN_SPLASH,
  SCREEN_MAIN_MENU,
  SCREEN_BLADE_MENU,
  SCREEN_CHANNEL_MENU
}enum_menu;


typedef enum
{
    FIRST_BLADE = 0,
    BLADE_1 = FIRST_BLADE,
    BLADE_2,
    BLADE_3,
    BLADE_4,
    MAX_NUM_BLADE
}enum_blade_id;

typedef enum
{
  CHANNEL_1,
  CHANNEL_2,
  MAX_NUM_CHANNEL
}enum_channel_id;
typedef enum
{
    NO_TAG_TOUCHED,
    BUTTON_TAG_BLADE_1,
    BUTTON_TAG_BLADE_2,
    BUTTON_TAG_BLADE_3,
    BUTTON_TAG_BLADE_4,
    BUTTON_TAG_CHANNEL_1,
    BUTTON_TAG_CHANNEL_2,
    BUTTON_TAG_BACK,
    BUTTON_TAG_HOME,
    SLIDER_INTENSITY
}enum_tag_id;

extern uint32_t tag;


void ui_draw_splash_screen(void);
void ui_draw_main_menu(void);
void ui_draw_blade_button(enum_blade_id blade_number);
void ui_clear_screen(void);
void ui_get_calibrate_parameter();
void ui_set_calibrate_parameter();
uint8_t ui_get_touched_tag_id();
void ui_draw_blade_menu(uint8_t blade_id);
void draw_channel_menu(uint8_t blade_id, uint8_t channel_id);
void ui_set_new_screen(uint8_t button_tag);
void ui_process_new_button_touched(uint8_t tag);
void ui_draw_previous_screen();
void ui_draw_current_screen();
uint8_t ui_is_not_respond();
void ui_process_slider(uint8_t tag);

#endif
