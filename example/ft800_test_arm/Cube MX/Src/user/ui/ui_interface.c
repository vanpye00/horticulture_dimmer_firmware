#include "stm32f3xx_hal.h"
#include "ft800.h"
#include "ui_interface.h"
#include "timer_interface.h"
#include "string.h"

uint32_t tag_x, tag_y;
uint8_t current_menu = SCREEN_MAIN_MENU;
uint8_t current_channel_selection;
uint8_t current_blade_selection;
uint16_t dim_intensity[MAX_NUM_BLADE-1][MAX_NUM_CHANNEL];
uint16_t slider_percent;
uint32_t ui_calibrated_params[6] =
{
  32968,
  4294966482,
  4294240994,
  4294966555,
  4294944437,
  20890917,
};

void ui_draw_splash_screen(void)
{
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
    
	FT_SET_COLOR(COLOR_BLACK);
	
  cmd_text(DISPLAY_CENTER_X,DISPLAY_CENTER_Y-15, 30,OPT_CENTER, "Blade Dimmer");
  cmd_text(DISPLAY_CENTER_X,DISPLAY_CENTER_Y + 15, 23,OPT_CENTER, "Rev 1.0");

	cmd(DISPLAY());
	cmd(CMD_SWAP);	    

  delay_ms(1000);
}

void ui_draw_main_menu(void)
{
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
    
	FT_SET_COLOR(COLOR_BLACK);
	cmd(TAG_MASK(1));
    //cmd_text(DISPLAY_WIDTH/4,DISPLAY_HEIGHT/4, 27,OPT_CENTER, "Blade 1");
    for (enum_blade_id i=FIRST_BLADE; i<MAX_NUM_BLADE; i++)
    {
        ui_draw_blade_button(i);
    }
	cmd(DISPLAY());
	cmd(CMD_SWAP);	    
  delay_ms(50);    
}

void ui_draw_blade_button(enum_blade_id blade_number)
{
    uint16_t x, y;
    uint16_t width = (DISPLAY_WIDTH/4 - 10)*2;
    uint16_t height = (DISPLAY_HEIGHT/4 - 10)*2;
    
    char blade_text[] ={'B','l','a','d','e',' ', '1'}; 
    switch (blade_number)
    {
        case BLADE_1:
            x = 10;
            y = 10;
            blade_text[strlen(blade_text)-1] += BLADE_1;
            cmd(TAG(BUTTON_TAG_BLADE_1));
            break;
        case BLADE_2:
            x = DISPLAY_WIDTH/2+10;
            y = 10;
            blade_text[strlen(blade_text)-1] += BLADE_2;        
            cmd(TAG(BUTTON_TAG_BLADE_2));
            break;
        case BLADE_3:
            x = 10;
            y = DISPLAY_HEIGHT/2+10;
            blade_text[strlen(blade_text)-1] += BLADE_3;        
            cmd(TAG(BUTTON_TAG_BLADE_3));
            break;
        case BLADE_4:
            x = DISPLAY_WIDTH/2+10;
            y = DISPLAY_HEIGHT/2+10;
            blade_text[strlen(blade_text)-1] += BLADE_4;        
            cmd(TAG(BUTTON_TAG_BLADE_4));
            break;
        default:
            break;
    }
    FT_FGCOLOR(COLOR_GRAY);
 	  cmd_button(x, y, width,height, 28,0, blade_text);
   
}

uint8_t ui_get_touched_tag_id()
{
    uint8_t id = HOST_MEM_RD32(REG_TOUCH_TAG);
    return id;
}

void ui_draw_blade_menu(uint8_t blade_id)
{
  uint16_t x = 5;
  uint16_t y = (DISPLAY_HEIGHT/3 - 5);
  uint16_t width = (DISPLAY_WIDTH/2 - 5);
  uint16_t height = (DISPLAY_HEIGHT/4 - 10);
  uint8_t tag_id;

	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
  char title[] = {'B','l','a','d','e',' ', '1'};
    
  title[strlen(title)-1] += blade_id;
    
	FT_SET_COLOR(COLOR_BLACK);
	cmd(TAG_MASK(1));
  cmd_text(DISPLAY_WIDTH/2,50, 30,OPT_CENTER, title);

  FT_FGCOLOR(COLOR_GRAY);
    
  cmd(TAG(BUTTON_TAG_CHANNEL_1));
 	cmd_button(x+25, y, width-50,height, 28,0, "Channel 1");
  cmd(TAG(BUTTON_TAG_CHANNEL_2));
 	cmd_button(x + DISPLAY_WIDTH/2+25, y, width-50,height, 27,0, "Channel 2");
    
  cmd(TAG(BUTTON_TAG_BACK));
 	cmd_button(5, DISPLAY_HEIGHT - height - 5, width,height, 27,0, "Back");
  
  cmd(TAG(BUTTON_TAG_HOME));
 	cmd_button(5+DISPLAY_WIDTH/2, DISPLAY_HEIGHT - height - 5, width,height, 27,0, "HOME");
    
	cmd(DISPLAY());
	cmd(CMD_SWAP);	    
  delay_ms(50);    
}

void ui_draw_channel_menu(uint8_t blade_id, uint8_t channel_id)
{
  uint16_t width = (DISPLAY_WIDTH/2 - 5);
  uint16_t height = (DISPLAY_HEIGHT/4 - 10);
  uint16_t intensity = dim_intensity[current_blade_selection][current_channel_selection];
  uint8_t second_digit = intensity/10;
  uint8_t first_digit = intensity - second_digit*10;
    
  char percent_text[] = {'1', '0', '0','%','\0'};
  if (intensity < 100)
  {
    percent_text[1] = first_digit + '0';
    percent_text[0] = second_digit + '0';
    percent_text[2] = '%';
    percent_text[3] = '\0';
  }
  
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
  char blade_title[] = {'B', 'l', 'a', 'd', 'e', ' ', '1'};
  char channel_title[] = {'C', 'h', 'a', 'n', 'n', 'e', 'l', ' ', '1'};
  blade_title[strlen(blade_title)-1] += blade_id;
  channel_title[strlen(channel_title)-1] += channel_id;

	FT_SET_COLOR(COLOR_BLACK);
	cmd(TAG_MASK(1));
  cmd_text(DISPLAY_WIDTH/2, 45, 30, OPT_CENTER, blade_title);    
  cmd_text(DISPLAY_WIDTH/2, 80, 23, OPT_CENTER, channel_title);    
  cmd_text(DISPLAY_WIDTH/2, 110, 23, OPT_CENTER, percent_text);    

  FT_FGCOLOR(COLOR_GRAY);

  cmd(TAG(BUTTON_TAG_BACK));
 	cmd_button(5, DISPLAY_HEIGHT - height - 5, width,height, 27,0, "Back");
  
  cmd(TAG(BUTTON_TAG_HOME));
 	cmd_button(5+DISPLAY_WIDTH/2, DISPLAY_HEIGHT - height - 5, width,height, 27,0, "HOME");

  cmd(TAG(SLIDER_INTENSITY));
  cmd_slider(DISPLAY_WIDTH/4, DISPLAY_HEIGHT/2 + 10 , DISPLAY_WIDTH/2, 10, 0, slider_percent, 100);
  
	cmd(DISPLAY());
	cmd(CMD_SWAP);	    
  delay_ms(50);    
}

void ui_clear_screen(void)
{
	cmd(CMD_DLSTART);
	cmd(CLEAR_COLOR_RGB(0,0,0));
	cmd(CLEAR(1,1,1));
	cmd(DISPLAY());
	cmd(CMD_SWAP);
}

void ui_get_calibrate_parameter()
{
  for (int i=0; i<6; i++)
  {
    ui_calibrated_params[i] = HOST_MEM_RD32(REG_TOUCH_TRANSFORM_A + i*4);
  }
}

void ui_set_calibrate_parameter()
{
  for (int i=0; i<6; i++)
  {
    HOST_MEM_WR32(REG_TOUCH_TRANSFORM_A + i*4, ui_calibrated_params[i]);
  }  
}

void ui_process_new_button_touched(uint8_t tag)
{
  switch(tag)
  {
    case BUTTON_TAG_BLADE_1:
    case BUTTON_TAG_BLADE_2:
    case BUTTON_TAG_BLADE_3:
    case BUTTON_TAG_BLADE_4:
      current_blade_selection = tag;
      current_menu = SCREEN_BLADE_MENU;
      ui_draw_blade_menu(current_blade_selection);
      break;
    case BUTTON_TAG_CHANNEL_1:
      current_menu = SCREEN_CHANNEL_MENU;
      current_channel_selection = CHANNEL_1;
      ui_draw_channel_menu(current_blade_selection, CHANNEL_1);
      break;
    case BUTTON_TAG_CHANNEL_2:
      current_menu = SCREEN_CHANNEL_MENU;
      current_channel_selection = CHANNEL_2;
      ui_draw_channel_menu(current_blade_selection, CHANNEL_2);
      break;
    case BUTTON_TAG_BACK:
      ui_draw_previous_screen();
      break;
    case BUTTON_TAG_HOME:
      ui_draw_main_menu();
      break;
    case SLIDER_INTENSITY:
      ui_process_slider(tag);
      break;
    default:
      break;
  }
  while(!cmd_ready());
}

void ui_draw_previous_screen()
{
  switch(current_menu)
  {
    case SCREEN_BLADE_MENU:
      current_menu = SCREEN_MAIN_MENU;
      ui_draw_main_menu();
      break;
    case SCREEN_CHANNEL_MENU:
      current_menu = SCREEN_BLADE_MENU;
      ui_draw_blade_menu(current_blade_selection);
    default:
      break;
  }
}

void ui_draw_current_screen()
{
  switch(current_menu)
  {
    case SCREEN_MAIN_MENU:
      ui_draw_main_menu();
    case SCREEN_BLADE_MENU:
      ui_draw_blade_menu(current_blade_selection);
      break;
    case SCREEN_CHANNEL_MENU:
      ui_draw_channel_menu(current_blade_selection, current_channel_selection);
    default:
      break;
  }
}

uint8_t ui_is_not_respond()
{
  uint8_t dev_id;
	//Read Dev ID
	dev_id = HOST_MEM_RD8(REG_ID);      // Read device id
	if(dev_id != 0x7C)                  // Device ID should always be 0x7C
	{   
		return 1;
	} 
  return 0;
}

void ui_process_slider(uint8_t tag)
{
  uint32_t xy;
  uint16_t slider_min_x = DISPLAY_WIDTH/4;
  uint16_t slider_max_x = DISPLAY_WIDTH*3/4;  
  uint16_t slider_width_x = slider_max_x-slider_min_x;
  while (ui_get_touched_tag_id() == tag)
  {
    xy = HOST_MEM_RD32(REG_TOUCH_TAG_XY);
    tag_x = xy >> 16;
    if (tag_x < slider_min_x)
    {
      tag_x = slider_min_x;
    } 
    else if (tag_x > slider_max_x)
    {
      tag_x = slider_max_x;
    }    
    slider_percent = (tag_x - slider_min_x)*100/slider_width_x;
    dim_intensity[current_blade_selection][current_channel_selection] = slider_percent;
    ui_draw_channel_menu(current_blade_selection, current_channel_selection);
  }
}

