#ifndef TIMER_INTERFACE_H
#define TIMER_INTERFACE_H

#include "stm32f3xx_hal.h"

#define delay_ms(__x__) HAL_Delay(__x__)

void Timer_Start(void);

#endif
