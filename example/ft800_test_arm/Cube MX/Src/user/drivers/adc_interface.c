#include "main_user.h"
#include "adc_interface.h"

uint16_t AnalogBuffer[16];

void ADC_Start(void)
{
  HAL_ADC_Start_DMA(&hadc2, (uint32_t *)AnalogBuffer, 16);
}

  uint16_t max=0;
  uint16_t min=0;
uint16_t ADC_Get_ZCD(void)
{
  
  htim1.Instance->CNT = 0;
  
  while(htim1.Instance->CNT <= 50000)
  {
    for (uint8_t i=0; i<16;i++)
    {
      if (AnalogBuffer[i] >= max)
      {
        max = AnalogBuffer[i];
      }
    }
  }
  min = max;
  htim1.Instance->CNT = 0;
  while(htim1.Instance->CNT <= 50000)
  {
    for (uint8_t i=0; i<16;i++)
    {
      if (AnalogBuffer[i] <= min)
      {
        min = AnalogBuffer[i];
      }
    }    
  }
  return (max-min)/2;
}

