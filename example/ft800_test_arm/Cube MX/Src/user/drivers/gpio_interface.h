#ifndef GPIO_INTERFACE_H
#define GPIO_INTERFACE_H

#include "main.h"

#define IO_PIN_SPI_CS PIN_SPI1_CS_GPIO_Port, PIN_SPI1_CS_Pin
#define IO_PIN_FT_PD PIN_FT_PDN_GPIO_Port, PIN_FT_PDN_Pin

#define IO_SetPin(__pin__) HAL_GPIO_WritePin(__pin__, GPIO_PIN_SET)
#define IO_ClearPin(__pin__) HAL_GPIO_WritePin(__pin__, GPIO_PIN_RESET)
#define IO_ReadPin(__pin__) HAL_GPIO_ReadPin(__pin__)

#endif