#include "stm32f3xx_hal.h"
#include "spi_interface.h"
#include "main_user.h"

#define DEFAULT_SPI_TIMEOUT 5000 //5 seconds

uint8_t rx_buff;

char SPI_send(char data)
{
    HAL_SPI_TransmitReceive(&hspi1 , (uint8_t *) &data, &rx_buff, sizeof(char), DEFAULT_SPI_TIMEOUT);
    return (char) rx_buff;
}