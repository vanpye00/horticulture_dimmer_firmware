#ifndef ADC_INTERFACE_H
#define ADC_INTERFACE_H

extern uint16_t AnalogBuffer[16];

void      ADC_Start(void);
uint16_t  ADC_Get_ZCD(void);
#endif