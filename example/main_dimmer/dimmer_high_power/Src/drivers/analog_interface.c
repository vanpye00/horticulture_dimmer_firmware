#include "stm32f3xx_hal.h"
#include "analog_interface.h"

#define ADC_CONVERTED_DATA_BUFFER_SIZE ((uint32_t) 32) 
#define MAX_ADC_PERIPHERAL 2
#define PERIPHERAL_ADC2 0

ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

static uint16_t   aADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE];

void init_adc(ADC_TypeDef *adc_type, ADC_HandleTypeDef *hadc);
void config_adc_channel(ADC_HandleTypeDef *hadc, uint32_t channel);
void start_adc(ADC_HandleTypeDef *hadc);
void ADC_Error_Handler();

void init_adc(ADC_TypeDef *adc_type, ADC_HandleTypeDef *hadc)
{   
    hadc->Instance = adc_type;
    if (HAL_ADC_DeInit(hadc) != HAL_OK)
    {
        /* ADC de-initialization Error */
        ADC_Error_Handler();
    }    
    hadc->Init.ClockPrescaler        = ADC_CLOCK_SYNC_PCLK_DIV2;      /* Synchronous clock mode, input ADC clock divided by 2*/
    hadc->Init.Resolution            = ADC_RESOLUTION_12B;            /* 12-bit resolution for converted data */
    hadc->Init.DataAlign             = ADC_DATAALIGN_RIGHT;           /* Right-alignment for converted data */
    hadc->Init.ScanConvMode          = DISABLE;                       /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
    hadc->Init.EOCSelection          = ADC_EOC_SINGLE_CONV;           /* EOC flag picked-up to indicate conversion end */
    hadc->Init.LowPowerAutoWait      = DISABLE;                       /* Auto-delayed conversion feature disabled */
    hadc->Init.ContinuousConvMode    = ENABLE;                        /* Continuous mode enabled (automatic conversion restart after each conversion) */
    hadc->Init.NbrOfConversion       = 1;                             /* Parameter discarded because sequencer is disabled */
    hadc->Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
    hadc->Init.NbrOfDiscConversion   = 1;                             /* Parameter discarded because sequencer is disabled */
    hadc->Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Software start to trig the 1st conversion manually, without external event */
    hadc->Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE; /* Parameter discarded because software trigger chosen */
    hadc->Init.DMAContinuousRequests = ENABLE;                        /* ADC DMA continuous request to match with DMA circular mode */
    hadc->Init.Overrun               = ADC_OVR_DATA_OVERWRITTEN;      /* DR register is overwritten with the last conversion result in case of overrun */
    /* Initialize ADC peripheral according to the passed parameters */
    if (HAL_ADC_Init(hadc) != HAL_OK)
    {
        ADC_Error_Handler();
    }    
    /* ### - 2 - Start calibration ############################################ */
    if (HAL_ADCEx_Calibration_Start(hadc, ADC_SINGLE_ENDED) !=  HAL_OK)
    {
        ADC_Error_Handler();
    }
}

void config_adc_channel(ADC_HandleTypeDef *hadc, uint32_t channel)
{
    ADC_ChannelConfTypeDef  sConfig;
    /* ### - 3 - Channel configuration ######################################## */
    sConfig.Channel      = channel;                /* Sampled channel number */
    sConfig.Rank         = ADC_REGULAR_RANK_1;          /* Rank of sampled channel number ADCx_CHANNEL */
    sConfig.SamplingTime = ADC_SAMPLETIME_61CYCLES_5;   /* Sampling time (number of clock cycles unit) */
    sConfig.SingleDiff   = ADC_SINGLE_ENDED;            /* Single-ended input channel */
    sConfig.OffsetNumber = ADC_OFFSET_NONE;             /* No offset subtraction */ 
    sConfig.Offset = 0;                                 /* Parameter discarded because offset correction is disabled */
    if (HAL_ADC_ConfigChannel(hadc, &sConfig) != HAL_OK)
    {
        ADC_Error_Handler();
    }   

}

void start_adc(ADC_HandleTypeDef *hadc)
{
    /* ### - 4 - Start conversion in DMA mode ################################# */
    if (HAL_ADC_Start_DMA(hadc,
                        (uint32_t *)aADCxConvertedData,
                        ADC_CONVERTED_DATA_BUFFER_SIZE
                       ) != HAL_OK)
    {
        ADC_Error_Handler();
    }     
}



void ADC_Error_Handler()
{
    while(1);
}