#include "stm32f3xx_hal.h"
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;

extern void init_adc(ADC_TypeDef *adc_type, ADC_HandleTypeDef *hadc);
extern void config_adc_channel(ADC_HandleTypeDef *hadc, uint32_t channel);
extern void start_adc(ADC_HandleTypeDef *hadc);