#include "dimmer_control.h"
#include "tim.h"
#include "timer_interface.h"
#include "gpio_interface.h"
  
dimmer_channel_t DimmerChannelInfo[MAX_NUM_DIMMER_CHANNEL] = 
{
//IsPresent ID        htim    timer dim level  dim ticks full on  cal power   dim style    corr percent corr ticks
  {false, CHANNEL_1A, &htim1, TIM1, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_1B, &htim2, TIM2, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE},
  {false, CHANNEL_2A, &htim3, TIM3, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_2B, &htim4, TIM4, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_3A, &htim4, TIM4, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_3B, &htim4, TIM4, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_4A, &htim4, TIM4, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
  {false, CHANNEL_4B, &htim4, TIM4, 0.0,        0,        0,        0,        DIM_FORWARD, 0.0,         0,        SCHEDULE_NONE}, 
};

static void GetActiveChannels();

void DimmerControl_Init(void)
{
  GetActiveChannels();
}

static void GetActiveChannels()
{
  if (Gpio_ReadPin(IO_PIN_SENSE1))
  {
    DimmerChannelInfo[CHANNEL_1A].isPresent = 1;
    DimmerChannelInfo[CHANNEL_1B].isPresent = 1;
  }
  if (Gpio_ReadPin(IO_PIN_SENSE2))
  {
    DimmerChannelInfo[CHANNEL_2A].isPresent = 1;
    DimmerChannelInfo[CHANNEL_2B].isPresent = 1;
  }
}

dimmer_channel_t DimmerControl_GetChannelInfo(enum_channel_id channel)
{
    return DimmerChannelInfo[channel];
}

void DimmerControl_SetChannelInfo(enum_channel_id channel, dimmer_channel_t *channelInfo)
{
  bool isTimerStartNeeded = false;
  uint32_t dimTicks;
  
  if (channelInfo->dimLevelPercent == 0.0)
  {
    HAL_TIM_Base_Stop_IT(channelInfo->htim);
  }
  else if (DimmerChannelInfo[channel].dimLevelPercent == 0)
  {
    isTimerStartNeeded = true;
  }

  
  memcpy( &DimmerChannelInfo[channel], channelInfo, sizeof(dimmer_channel_t));
  dimTicks = (uint32_t)(DimmerChannelInfo[channel].dimLevelPercent * 10) * MAX_DIMMING_PERIOD / 1000;
  DimmerChannelInfo[channel].dimLevelInTimerTicks = dimTicks;
  
  dimTicks = (uint32_t)(DimmerChannelInfo[channel].dimCorrectionPercent * 10) * MAX_DIMMING_PERIOD / 1000;
  if (DimmerChannelInfo[channel].dimStyle == DIM_FORWARD)
  {
  }
  else if (DimmerChannelInfo[channel].dimStyle == DIM_BACKWARD)
  {
    dimTicks = MAX_DIMMING_PERIOD - DimmerChannelInfo[channel].dimLevelInTimerTicks + dimTicks;
  }
  else if(DimmerChannelInfo[channel].dimStyle == DIM_CENTER)
  {
    dimTicks = (MAX_DIMMING_PERIOD - DimmerChannelInfo[channel].dimLevelInTimerTicks + dimTicks)/2;
  }
  else
  {
    dimTicks = 0;
  }
  DimmerChannelInfo[channel].dimCorrectionInTimerTicks = dimTicks;

  if (isTimerStartNeeded)
  {
    HAL_TIM_Base_Start_IT(channelInfo->htim);
  }
}