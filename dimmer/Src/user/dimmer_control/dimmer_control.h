#ifndef DIMMER_CONTROL_H
#define DIMMER_CONTROL_H

#include <math.h>
#include <stdInt.h>
#include <stdbool.h>
#include "tim.h"

#define MIN_DIMMING_DELAY_START 2000
#define MAX_DIMMING_PERIOD 29000

#define DIMMING_UI_NORMAL_CONTROL_STEP .5
#define DIMMING_UI_FAST_CONTROL_STEP 5

#define DIMMING_UI_NORMAL_POWER_STEP 10
#define DIMMING_UI_FAST_POWER_STEP 100

typedef enum
{
    FIRST_BLADE = 0,
    CHANNEL_1A = FIRST_BLADE,
    CHANNEL_1B,
    CHANNEL_2A,
    CHANNEL_2B,
    CHANNEL_3A,
    CHANNEL_3B,
    CHANNEL_4A,
    CHANNEL_4B,
    MAX_NUM_DIMMER_CHANNEL
}enum_channel_id;

typedef enum
{
  DIM_FORWARD,
  DIM_BACKWARD,
  DIM_CENTER,
  MAX_NUM_DIM_STYLE,
}dim_style_t;

typedef enum
{
  SCHEDULE_NONE,
  SCHEDULE_ON,
}schedule_type_t;

typedef struct
{
  bool isPresent; 
  enum_channel_id channel_id;  
  TIM_HandleTypeDef *htim;
  TIM_TypeDef *timer;
  float_t dimLevelPercent;
  uint32_t dimLevelInTimerTicks;
  uint16_t fullOnPower;
  uint16_t calculatedPower;
  dim_style_t dimStyle;
  float_t dimCorrectionPercent;
  uint32_t dimCorrectionInTimerTicks;
  schedule_type_t schedule;
}dimmer_channel_t;

extern dimmer_channel_t DimmerChannelInfo[];

dimmer_channel_t DimmerControl_GetChannelInfo(enum_channel_id channel);
void DimmerControl_SetChannelInfo(enum_channel_id channel, dimmer_channel_t *channelInfo);
void DimmerControl_Init(void);

#endif
