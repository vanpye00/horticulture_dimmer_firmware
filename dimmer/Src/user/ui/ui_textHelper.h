#ifndef UI_TEXT_HELPER_H
#define UI_TEXT_HELPER_H

#include "math.h"
#include "stdInt.h"
#include "dimmer_control.h"

#define MAX_TEXT_HELPER_STRING_SIZE 8U

typedef enum
{
  NUMBER_TYPE_FLOAT,
  NUMBER_TYPE_INT,
  NUMBER_TYPE_UINT16,
}number_type_t;

int UI_TextHelper_GetTextFromNumber(void *number, char *returnText, number_type_t numberType);

void UI_TextHelper_GetDimLevelText(float_t number, char *returnText);
void UI_TextHelper_GetCalPowerText(uint16_t number, char *returnText);
void UI_TextHelper_GetStyleText(dim_style_t dimStyle, char *returnText);
void UI_TextHelper_GetCorrectionText(float_t number, char *returnText);

#endif
