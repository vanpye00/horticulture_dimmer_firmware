#ifndef UI_INTERFACE_H
#define UI_INTERFACE_H

#include <math.h>
#include <stdBool.h>
#include "dimmer_control.h"

#define DISPLAY_HEIGHT (uint32_t)272
#define DISPLAY_WIDTH (uint32_t)480
#define DISPLAY_CENTER_X (DISPLAY_WIDTH/2)
#define DISPLAY_CENTER_Y (DISPLAY_HEIGHT/2)

//Color Commands
#define GET_RGB_R(__COLOR__) (__COLOR__>>16)
#define GET_RGB_G(__COLOR__) ((__COLOR__>>8)&0xff)
#define GET_RGB_B(__COLOR__) (__COLOR__&0xff)
#define FT_CLEAR_RGB(__COLOR__) cmd(CLEAR_COLOR_RGB(GET_RGB_R(__COLOR__),GET_RGB_G(__COLOR__),GET_RGB_B(__COLOR__)))
#define FT_CLEAR(__COLOR__, __STENCIL__, __TAG__) cmd(CLEAR(__COLOR__, __STENCIL__, __TAG__))
#define FT_CLEAR_BUFFER() FT_CLEAR(CLEAR_COLOR_BUFFER, CLEAR_STENCIL_BUFFER, CLEAR_TAG_BUFFER)
#define FT_FGCOLOR(__COLOR__) cmd_fgcolor(__COLOR__)

//Clear buffer parameters
#define CLEAR_COLOR_BUFFER 1
#define CLEAR_STENCIL_BUFFER 1
#define CLEAR_TAG_BUFFER 1
#define KEEP_COLOR_BUFFER 1
#define KEEP_STENCIL_BUFFER 1
#define KEEP_TAG_BUFFER 1

#define DEFAULT_BACKGROUND COLOR_WHITE
#define COLOR_WHITE 0xffffff
#define COLOR_BLACK 0
#define COLOR_LIGHT_GRAY 0xD3D3D3
#define COLOR_GRITTER 0xE6F2FF
#define COLOR_GRAY    0x696969	
#define COLOR_BLUE    0x00008B
#define COLOR_GREEN   0x008000

typedef enum
{
  SCREEN_ID_SPLASH,
  SCREEN_ID_MAIN_MENU,
  SCREEN_ID_CHANNEL_MENU,
  MAX_NUM_SCREEN_ID,
}enum_screen_id;


typedef enum
{
    NO_TAG_TOUCHED,
    BUTTON_TAG_MAIN_MENU_TOP_LEFT,
    BUTTON_TAG_MAIN_MENU_TOP_RIGHT,
    BUTTON_TAG_MAIN_MENU_BOT_LEFT,
    BUTTON_TAG_MAIN_MENU_BOT_RIGHT,
    BUTTON_TAG_CHANNEL_MENU_HOME,
    BUTTON_TAG_CHANNEL_MENU_SCHEDULE,
    BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL,
    BUTTON_TAG_CHANNEL_MENU_POWER,
    BUTTON_TAG_CHANNEL_MENU_SCHEDULE_ENABLE,
    BUTTON_TAG_CHANNEL_MENU_DIM_STYLE,
    BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION,
    BUTTON_TAG_CHANNEL_MENU_PLUS,
    BUTTON_TAG_CHANNEL_MENU_MINUS,
    SLIDER_INTENSITY,
    UNUSED_TAG_ID,
}enum_tag_id;

typedef struct
{
  uint8_t blade_id;
  uint8_t channel_id;
  float_t dimPercent;
  uint8_t scheduleType;
  uint16_t power;
  uint8_t dimCorrection;
  uint8_t dimStyle;
  bool isPresent;
}dimmmer_channel_t;

typedef enum
{
  CHANNEL_MENU_NO_ACTION,
  CHANNEL_MENU_ACTION_PLUS,
  CHANNEL_MENU_ACTION_MINUS,
}channel_menu_action_t;
extern uint32_t tag;


void UI_DrawSplashScreen(void);
void UI_DrawNewScreen(enum_screen_id screenId);
void UI_DrawMainBladeBox(void);
void UI_UpdateMainScreen(void);
void UI_UpdateScreen(void);
void UI_DrawChannelMenu(void);
void UI_UpdateChannelMenu(void);

void UI_UpdateDimmerChannelInfo(void);

void UI_ClearScreen(void);
uint8_t UI_IsScreenReplying(void);

void ui_get_calibrate_parameter();
void UI_SetCalibrateParameter();

void ui_draw_blade_menu(uint8_t blade_id);
void draw_channel_menu(uint8_t blade_id, uint8_t channel_id);
void ui_set_new_screen(uint8_t button_tag);
void ui_process_new_button_touched(uint8_t tag);
void ui_draw_previous_screen();
void ui_draw_current_screen();
void ui_process_slider(uint8_t tag);
#endif
