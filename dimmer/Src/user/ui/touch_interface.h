#ifndef TOUCH_INTERFACE_H
#define TOUCH_INTERFACE_H

typedef enum
{
  NO_TOUCH,
  TOUCH_A_POINT,
  TOUCH_SWIPE_UP,
  TOUCH_SWIPE_DOWN,
  TOUCH_AND_HOLD,
  MAX_TOUCH_ACTION,
}touch_type_t;

typedef struct
{
  vertex_point_t point;
  touch_type_t touchType;
}touch_info_t;

touch_info_t Touch_GetTouchXY(void);
void Touch_ResetTouchUpdateTime(void);
uint8_t Touch_GetTouchTagId(void);

#endif
