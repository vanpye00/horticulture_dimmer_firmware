#include <stdInt.h>
#include <stdBool.h>
#include "ft800.h"
#include "touch_interface.h"

#define TOUCH_UPDATE_PERIOD 30 //30ms
#define TOUCH_IDE_VALUE 0x8000
#define MIN_TOUCH_Y_SWIPE_THRESHOLD 10
#define MIN_TOUCH_X_SWIPE_THRESHOLD 10

static touch_type_t TouchType = NO_TOUCH;
static uint32_t LastTouchUpdate = 0;
static vertex_point_t startTouchPoint;
static vertex_point_t lastTouchPoint;
static bool foundTouchStart = 0;

touch_info_t Touch_GetTouchXY(void)
{
  uint32_t touchXY;
  uint16_t touchX;
  uint16_t touchY;
  touch_info_t touchInfo;
  
  if (HAL_GetTick() - LastTouchUpdate >= TOUCH_UPDATE_PERIOD)
  {
    LastTouchUpdate = HAL_GetTick();
    touchXY = HOST_MEM_RD32(REG_TOUCH_SCREEN_XY);

    touchY = touchXY & 0xffff;
    touchX = (touchXY >> 16) & 0xffff;
    if (touchX != TOUCH_IDE_VALUE)
    {
      if (foundTouchStart==0)
      {
        foundTouchStart = 1;
        startTouchPoint.y = touchY;
        startTouchPoint.x = touchX;
      }
      else
      {
        TouchType = TOUCH_AND_HOLD;
        touchInfo.point.x = lastTouchPoint.x;
        touchInfo.point.y = lastTouchPoint.y;
      }
      lastTouchPoint.x = touchX;      
      lastTouchPoint.y = touchY;      
    }
    else
    {
      if (foundTouchStart == 1)
      {
        foundTouchStart = 0;
        if (lastTouchPoint.x > startTouchPoint.x )
        {
          if (lastTouchPoint.x - startTouchPoint.x > MIN_TOUCH_X_SWIPE_THRESHOLD)
          {
            TouchType = TOUCH_SWIPE_UP;
          }
          else
          {
            TouchType = TOUCH_A_POINT;
            touchInfo.point.x = lastTouchPoint.x;
            touchInfo.point.y = lastTouchPoint.y;
          }
        }
        else
        {
          if (startTouchPoint.x - lastTouchPoint.x > MIN_TOUCH_X_SWIPE_THRESHOLD)
          {
            TouchType = TOUCH_SWIPE_DOWN;
          }        
          else
          {
            TouchType = TOUCH_A_POINT;
            touchInfo.point.x = lastTouchPoint.x;
            touchInfo.point.y = lastTouchPoint.y;
          }

        }
      }
      else
      {
        TouchType = NO_TOUCH;
      }
    }
  }
  touchInfo.touchType = TouchType;
  return touchInfo;
}

void Touch_ResetTouchUpdateTime(void)
{
  LastTouchUpdate = HAL_GetTick();
}

uint8_t Touch_GetTouchTagId(void)
{
  uint8_t id = HOST_MEM_RD32(REG_TOUCH_TAG);
  return id;
}
