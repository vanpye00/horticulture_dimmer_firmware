#include "ui_textHelper.h"
#include "stdInt.h"
#include "dimmer_control.h"

int UI_TextHelper_GetTextFromNumber(void *number, char *returnText, number_type_t numberType)
{
  switch (numberType)
  {
  case NUMBER_TYPE_FLOAT:
    return snprintf(returnText, MAX_TEXT_HELPER_STRING_SIZE, "%.1f", *((float_t*) number)); 
  case NUMBER_TYPE_UINT16:
    return snprintf(returnText, MAX_TEXT_HELPER_STRING_SIZE, "%d", *((uint16_t*) number)); 
  default:
    return 0;
  }
}

void UI_TextHelper_GetDimLevelText(float_t number, char *returnText)
{
  uint16_t numChars;
  numChars = UI_TextHelper_GetTextFromNumber(&number, returnText, NUMBER_TYPE_FLOAT);
  returnText[numChars] = '%';
  returnText[numChars+1] = '\0';
}

void UI_TextHelper_GetCalPowerText(uint16_t number, char *returnText)
{
  uint16_t numChars;
  numChars = UI_TextHelper_GetTextFromNumber(&number, returnText, NUMBER_TYPE_UINT16);
  returnText[numChars] = 'W';
  returnText[numChars+1] = '\0';
}

void UI_TextHelper_GetScheduleText(schedule_type_t scheduleType, char *returnText)
{
  switch (scheduleType)
  {
  case SCHEDULE_NONE:
    returnText[0] = 'N';
    returnText[1] = 'o';
    returnText[2] = 'n';
    returnText[3] = 'e';
    returnText[4] = '\0';
    break;
  case SCHEDULE_ON:
    returnText[0] = 'O';
    returnText[1] = 'n';
    returnText[2] = '\0';
    break;
  default:
    break;
  }
}

void UI_TextHelper_GetStyleText(dim_style_t dimStyle, char *returnText)
{
  switch (dimStyle)
  {
  case DIM_FORWARD:
    returnText[0] = 'F';
    returnText[1] = 'o';
    returnText[2] = 'r';
    returnText[3] = '\0';
    break;
  case DIM_BACKWARD:
    returnText[0] = 'R';
    returnText[1] = 'e';
    returnText[2] = 'v';
    returnText[3] = '\0';
    break;
  case DIM_CENTER:
    returnText[0] = 'C';
    returnText[1] = 'e';
    returnText[2] = 'n';
    returnText[3] = '\0';
    break;
  default:
    break;
  }
}

void UI_TextHelper_GetCorrectionText(float_t number, char *returnText)
{
  uint16_t numChars;
  numChars = UI_TextHelper_GetTextFromNumber(&number, returnText, NUMBER_TYPE_FLOAT);
  returnText[numChars] = '%';
  returnText[numChars+1] = '\0';
}