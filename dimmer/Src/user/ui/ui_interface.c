#include "stm32f3xx_hal.h"
#include "ft800.h"
#include "ui_interface.h"
#include "timer_interface.h"
#include "string.h"
#include "splash_screen_image_bolt_text.h"
#include "splash_screen_image_cow.h"
#include "touch_interface.h"
#include "splashscreen.h"
#include "ui_texthelper.h"

static uint32_t TagCoordinateX, TagCordinateY;
static uint8_t current_channel_selection;
static uint8_t current_blade_selection;
static uint16_t dim_intensity[MAX_NUM_DIMMER_CHANNEL-1];
static uint16_t slider_percent;

static enum_screen_id ActiveScreenId;
void SetActiveScreen(enum_screen_id screenId);

static enum_channel_id MainMenuActiveChannel[4] = {CHANNEL_1A, CHANNEL_1B, CHANNEL_2A, CHANNEL_2B};
static enum_channel_id ChannelMenuActiveChannel;
static enum_tag_id ChannelMenuActiveButton;
static uint32_t UICalibratedParams[6] =
{
  32968,
  4294966482,
  4294240994,
  4294966555,
  4294944437,
  20890917,
};


void UI_DrawSplashScreen(void)
{
  FT_WriteRawJpegToRam(RAM_G, SplashScreenImageCow, SPLASH_SCREEN_IMAGE_COW_TOTAL_SIZE);
  FT_WriteRawJpegToRam(RAM_G+SPLASH_SCREEN_IMAGE_COW_TOTAL_SIZE, SplashScreenImageBoltText, SPLASH_SCREEN_IMAGE_BOLT_TEXT_TOTAL_SIZE);
	FT_CLEAR_BUFFER();
  uint32_t tickStart;
  for (uint16_t i = 0; i < 1; i++)
  {
    tickStart = HAL_GetTick();
    cmd(CMD_DLSTART);
    FT_CLEAR_RGB(DEFAULT_BACKGROUND);
    cmd(CLEAR(1,1,1));
    cmd(BITMAP_HANDLE(5));
    cmd(BITMAP_SOURCE(RAM_G + SPLASH_SCREEN_IMAGE_COW_TOTAL_SIZE));
    cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH*2, SPLASH_SCREEN_IMAGE_BOLT_TEXT_HEIGHT));
    cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH, SPLASH_SCREEN_IMAGE_BOLT_TEXT_HEIGHT)); 
    cmd(BITMAP_HANDLE(6));
    cmd(BITMAP_SOURCE(RAM_G));
    cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_COW_WIDTH*2, SPLASH_SCREEN_IMAGE_COW_HEIGHT));
    cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, SPLASH_SCREEN_IMAGE_COW_WIDTH, SPLASH_SCREEN_IMAGE_COW_HEIGHT)); 
    cmd(BEGIN(BITMAPS));
    cmd(VERTEX2II(240-SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH/2,150,5,0));
    cmd(VERTEX2II(240-SPLASH_SCREEN_IMAGE_COW_WIDTH/2,10,6,0));
    cmd(END());
    FT_SetColorRGB(COLOR_BLACK);
    if (i % 2 == 0)
    {
      cmd_text(235,200, 25,OPT_CENTER, "Power     Blade");
    }
    cmd(DISPLAY());
    cmd(CMD_SWAP);	
    while (HAL_GetTick() - tickStart < 600);
	}

  for (uint16_t i = 480; i >= 240; i-=10)
  {
    cmd(CMD_DLSTART);
    FT_CLEAR_RGB(DEFAULT_BACKGROUND);
    cmd(CLEAR(1,1,1));
    cmd(BITMAP_HANDLE(5));
    cmd(BITMAP_SOURCE(RAM_G + SPLASH_SCREEN_IMAGE_COW_TOTAL_SIZE));
    cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH*2, SPLASH_SCREEN_IMAGE_BOLT_TEXT_HEIGHT));
    cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH, SPLASH_SCREEN_IMAGE_BOLT_TEXT_HEIGHT)); 
    cmd(BITMAP_HANDLE(6));
    cmd(BITMAP_SOURCE(RAM_G));
    cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_COW_WIDTH*2, SPLASH_SCREEN_IMAGE_COW_HEIGHT));
    cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, SPLASH_SCREEN_IMAGE_COW_WIDTH, SPLASH_SCREEN_IMAGE_COW_HEIGHT)); 
    cmd(BEGIN(BITMAPS));
    cmd(VERTEX2II(240-SPLASH_SCREEN_IMAGE_BOLT_TEXT_WIDTH/2,150,5,0));
    cmd(VERTEX2II(240-SPLASH_SCREEN_IMAGE_COW_WIDTH/2,10,6,0));
    cmd(END());
    FT_SetColorRGB(COLOR_BLACK);
    cmd_text(i,120, 24,OPT_CENTER, "Farmer LED");
    cmd_text(235,200, 25,OPT_CENTER, "Power     Blade");
    cmd(DISPLAY());
    cmd(CMD_SWAP);	
	}
  delay_ms(1000);
}

void UI_DrawNewScreen(enum_screen_id screenId)
{
  SetActiveScreen(screenId);
  UI_ClearScreen();
  Timer_DelayMs(250);
  
  switch(screenId)
  {
    case SCREEN_ID_SPLASH:
      UI_DrawSplashScreen();
      break;
    case SCREEN_ID_MAIN_MENU:
      UI_DrawMainBladeBox();
      Timer_DelayMs(500);
      break;
    case SCREEN_ID_CHANNEL_MENU:
      UI_DrawChannelMenu();
      break;
    default:
      break;
  }
  Timer_DelayMs(500);
}

void UI_DrawMainBladeBox(void)
{
  enum_channel_id channel;
  ft_rectangle_t rect;
  uint32_t text_color;
  uint32_t rect_border_color;
  
  char text[] = {'B', 'l', 'a','d','e',' ','1',' ','C','h',' ', 'A','\0'};
  char display_text_buffer[8];
  
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
  
  FT_SetColorRGB(COLOR_BLACK);
	cmd(TAG_MASK(1));
  cmd_text(DISPLAY_WIDTH/2-20,20, 24,OPT_CENTER, "Dashboard");
  
  for (uint8_t i=0; i<4; i++)
  {
    channel = MainMenuActiveChannel[i];
    if (DimmerChannelInfo[channel].isPresent)
    {
      text_color = COLOR_BLACK;
      rect_border_color = COLOR_BLACK;
    }
    else
    {
      text_color = COLOR_GRAY;
      rect_border_color = COLOR_GRAY;
    }
    if (channel % 2 == 0) 
    {
      text[11] = 'A';      
    }
    else
    {
      text[11] = 'B';
    }
    
    text[6] = (channel / 2) + 1 + '0';
  
    rect.borderColor = rect_border_color;
    rect.borderWidth = 2;
    rect.cornerCurvature = 10;
    rect.innerColor = COLOR_WHITE;
    switch (i)
    {
        case 0:
          rect.corner1 = (vertex_point_t){20, 50};
          rect.corner2 = (vertex_point_t){225, 140};
          FT_DrawRectangle(rect);
          FT_SetColorRGB(text_color);
          cmd_text(120, 55, 23, OPT_CENTER, text);
          if (DimmerChannelInfo[channel].isPresent)
          {
            cmd_text(30, 62, 22, 0,  "Dim Level...................");
            UI_TextHelper_GetDimLevelText(DimmerChannelInfo[channel].dimLevelPercent, display_text_buffer);
            cmd_text(180, 62, 22, 0, display_text_buffer);  
            cmd_text(30, 78, 22, 0,  "Calculated Power......");
            UI_TextHelper_GetCalPowerText(DimmerChannelInfo[channel].calculatedPower, display_text_buffer);
            cmd_text(180, 78, 22, 0, display_text_buffer);  
            cmd_text(30, 94, 22, 0,  "Schedule....................");
            UI_TextHelper_GetScheduleText(DimmerChannelInfo[channel].schedule, display_text_buffer);
            cmd_text(180, 94, 22, 0, display_text_buffer);  
            cmd_text(30, 110, 22, 0, "Dim Style...................");
            UI_TextHelper_GetStyleText(DimmerChannelInfo[channel].dimStyle, display_text_buffer);
            cmd_text(180, 110, 22, 0, display_text_buffer);  
            cmd_text(30, 126, 22, 0, "Dim Correction..........");
            UI_TextHelper_GetCorrectionText(DimmerChannelInfo[channel].dimCorrectionPercent, display_text_buffer);
            cmd_text(180, 126, 22, 0, display_text_buffer); 
          }
          break;
        case 1:
          rect.corner1.x = 255;
          rect.corner1.y = 50;
          rect.corner2.x = 460; 
          rect.corner2.y = 140;
          FT_DrawRectangle(rect);
          FT_SetColorRGB(text_color);
          cmd_text(355, 55, 23, OPT_CENTER, text);
          if (DimmerChannelInfo[channel].isPresent)
          {          
            cmd_text(265, 62, 22, 0,  "Dim Level...................");
            cmd_text(265, 78, 22, 0,  "Calculated Power......");
            cmd_text(265, 94, 22, 0,  "Schedule....................");
            cmd_text(265, 110, 22, 0, "Dim Style...................");
            cmd_text(265, 126, 22, 0, "Dim Correction..........");
            UI_TextHelper_GetDimLevelText(DimmerChannelInfo[channel].dimLevelPercent, display_text_buffer);
            cmd_text(415, 62, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCalPowerText(DimmerChannelInfo[channel].calculatedPower, display_text_buffer);
            cmd_text(415, 78, 22, 0, display_text_buffer); 
            UI_TextHelper_GetScheduleText(DimmerChannelInfo[channel].schedule, display_text_buffer);
            cmd_text(415, 94, 22, 0, display_text_buffer);  
            UI_TextHelper_GetStyleText(DimmerChannelInfo[channel].dimStyle, display_text_buffer);
            cmd_text(415, 110, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCorrectionText(DimmerChannelInfo[channel].dimCorrectionPercent, display_text_buffer);
            cmd_text(415, 126, 22, 0, display_text_buffer);  
          
          }
          break;
        case 2:
          rect.corner1.x = 20;
          rect.corner1.y = 170;
          rect.corner2.x = 225; 
          rect.corner2.y = 260;
          FT_DrawRectangle(rect);
          FT_SetColorRGB(text_color);
          cmd_text(120, 175, 23, OPT_CENTER, text);
          if (DimmerChannelInfo[channel].isPresent)
          {
            cmd_text(30, 182, 22, 0,  "Dim Level...................");
            cmd_text(30, 198, 22, 0,  "Calculated Power......");
            cmd_text(30, 214, 22, 0,  "Schedule....................");
            cmd_text(30, 230, 22, 0, "Dim Style...................");
            cmd_text(30, 246, 22, 0, "Dim Correction..........");
            UI_TextHelper_GetDimLevelText(DimmerChannelInfo[channel].dimLevelPercent, display_text_buffer);
            cmd_text(180, 182, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCalPowerText(DimmerChannelInfo[channel].calculatedPower, display_text_buffer);
            cmd_text(180, 198, 22, 0, display_text_buffer); 
            UI_TextHelper_GetScheduleText(DimmerChannelInfo[channel].schedule, display_text_buffer);
            cmd_text(180, 214, 22, 0, display_text_buffer);  
            UI_TextHelper_GetStyleText(DimmerChannelInfo[channel].dimStyle, display_text_buffer);
            cmd_text(180, 230, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCorrectionText(DimmerChannelInfo[channel].dimCorrectionPercent, display_text_buffer);
            cmd_text(180, 246, 22, 0, display_text_buffer);  
          
          }
          break;
        case 3:
          rect.corner1.x = 255;
          rect.corner1.y = 170;
          rect.corner2.x = 460; 
          rect.corner2.y = 260;
          FT_DrawRectangle(rect);
          FT_SetColorRGB(text_color);
          cmd_text(355, 175, 23, OPT_CENTER, text);
          if (DimmerChannelInfo[channel].isPresent)
          {
            cmd_text(265, 182, 22, 0,  "Dim Level...................");
            cmd_text(265, 198, 22, 0,  "Calculated Power......");
            cmd_text(265, 214, 22, 0,  "Schedule....................");
            cmd_text(265, 230, 22, 0, "Dim Style...................");
            cmd_text(265, 246, 22, 0, "Dim Correction..........");
            UI_TextHelper_GetDimLevelText(DimmerChannelInfo[channel].dimLevelPercent, display_text_buffer);
            cmd_text(415, 182, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCalPowerText(DimmerChannelInfo[channel].calculatedPower, display_text_buffer);
            cmd_text(415, 198, 22, 0, display_text_buffer); 
            UI_TextHelper_GetScheduleText(DimmerChannelInfo[channel].schedule, display_text_buffer);
            cmd_text(415, 214, 22, 0, display_text_buffer);  
            UI_TextHelper_GetStyleText(DimmerChannelInfo[channel].dimStyle, display_text_buffer);
            cmd_text(415, 230, 22, 0, display_text_buffer);  
            UI_TextHelper_GetCorrectionText(DimmerChannelInfo[channel].dimCorrectionPercent, display_text_buffer);
            cmd_text(415, 246, 22, 0, display_text_buffer);          
          }
          break;
        default:
            break;
    }
  }
  FT_SetColorRGB(COLOR_WHITE);
  cmd(BITMAP_SOURCE(RAM_G));
  cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_COW_WIDTH*2, SPLASH_SCREEN_IMAGE_COW_HEIGHT));
  cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, 50, 50)); 
  cmd( BITMAP_TRANSFORM_A(1100) );
  cmd( BITMAP_TRANSFORM_E(1100) );

  cmd(BEGIN(BITMAPS));
  cmd(VERTEX2II(290,8,6,0));
  cmd(END());
    
  cmd(DISPLAY());
	cmd(CMD_SWAP);	    
  
}

void UI_UpdateMainScreen(void)
{
    touch_info_t touchInfo = Touch_GetTouchXY();
    enum_channel_id newChannel;
    bool isTouchWithAbox = false;
    
    if (touchInfo.touchType == TOUCH_SWIPE_UP)
    {
      if (MainMenuActiveChannel[0] < CHANNEL_3A)
      {      
        for (uint8_t i=0; i<4; i++)
        {
          MainMenuActiveChannel[i] += 2;
        }
      } 
      UI_ClearScreen();
      Timer_DelayMs(100);
      Touch_ResetTouchUpdateTime();
      UI_DrawMainBladeBox();

    }
    else if (touchInfo.touchType == TOUCH_SWIPE_DOWN)
    {
      if (MainMenuActiveChannel[0] > CHANNEL_1A)
      {
        for (uint8_t i=0; i<4; i++)
        {
          MainMenuActiveChannel[i] -= 2;
        }
      }
  
      UI_ClearScreen();
      Timer_DelayMs(100);
      Touch_ResetTouchUpdateTime();
      UI_DrawMainBladeBox();
    }
    else if (touchInfo.touchType == TOUCH_A_POINT)
    {
      if (touchInfo.point.x >= 20 && touchInfo.point.y >= 50 && touchInfo.point.x <= 225 && touchInfo.point.y <= 140)
      {
        ChannelMenuActiveChannel = MainMenuActiveChannel[0];
        isTouchWithAbox = true;
      }
      else if (touchInfo.point.x >= 255 && touchInfo.point.y >= 50 && touchInfo.point.x <= 460 && touchInfo.point.y <= 140)
      {
        ChannelMenuActiveChannel = MainMenuActiveChannel[1];
        isTouchWithAbox = true;
      }
      else if (touchInfo.point.x >= 20 && touchInfo.point.y >= 170 && touchInfo.point.x <= 225 && touchInfo.point.y <= 260)
      {
        ChannelMenuActiveChannel = MainMenuActiveChannel[2];
        isTouchWithAbox = true;
      }
      else if (touchInfo.point.x >= 255 && touchInfo.point.y >= 170 && touchInfo.point.x <= 460 && touchInfo.point.y <= 260)
      {
        ChannelMenuActiveChannel = MainMenuActiveChannel[3];
        isTouchWithAbox = true;
      }
      if (isTouchWithAbox && DimmerChannelInfo[ChannelMenuActiveChannel].isPresent)
      {
        UI_ClearScreen();
        Timer_DelayMs(100);
        Touch_ResetTouchUpdateTime();
        UI_DrawNewScreen(SCREEN_ID_CHANNEL_MENU);
      }
    }
    else
    {
      UI_DrawMainBladeBox();
    }
}

void ui_draw_blade_menu(uint8_t blade_id)
{
}

void ui_draw_channel_menu(uint8_t blade_id, uint8_t channel_id)
{
      
}

void UI_ClearScreen(void)
{
	cmd(CMD_DLSTART);
	cmd(CLEAR_COLOR_RGB(255,255,255));
	cmd(CLEAR(1,1,1));
	cmd(DISPLAY());
	cmd(CMD_SWAP);
}

void ui_get_calibrate_parameter()
{
  for (int i=0; i<6; i++)
  {
    UICalibratedParams[i] = HOST_MEM_RD32(REG_TOUCH_TRANSFORM_A + i*4);
  }
}

void UI_SetCalibrateParameter()
{
  for (int i=0; i<6; i++)
  {
    HOST_MEM_WR32(REG_TOUCH_TRANSFORM_A + i*4, UICalibratedParams[i]);
  }  
}

void ui_process_new_button_touched(uint8_t tag)
{
}

void ui_draw_previous_screen()
{
}

void ui_draw_current_screen()
{
}

uint8_t UI_IsScreenReplying()
{
  uint8_t dev_id;
	//Read Dev ID
	dev_id = HOST_MEM_RD8(REG_ID);      // Read device id
	if(dev_id != 0x7C)                  // Device ID should always be 0x7C
	{   
		return 1;
	} 
  return 0;
}

void ui_process_slider(uint8_t tag)
{

}

void SetActiveScreen(enum_screen_id screenId)
{
  ActiveScreenId = screenId;
}

void UI_DrawChannelMenu(void)
{
  enum_channel_id channel;
  ft_rectangle_t rect;
  uint32_t text_color;
  uint32_t rect_border_color;
  
  char text[] = {'B', 'l', 'a','d','e',' ','1',' ','C','h',' ', 'A','\0'};
  char display_text_buffer[8];
  
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
  
  FT_SetColorRGB(COLOR_BLACK);
  
    channel = ChannelMenuActiveChannel;
      text_color = COLOR_BLACK;

    if (channel % 2 == 0) 
    {
      text[11] = 'A';      
    }
    else
    {
      text[11] = 'B';
    }
    text[6] = (channel / 2) + 1 + '0';
  cmd_text(DISPLAY_WIDTH/2-40,20, 24,OPT_CENTER, text);
  


    FT_SetColorRGB(text_color);
    uint8_t text_origin_x = 30;
    uint8_t text_origin_y = 40;
    uint8_t text_spacing = 35;
    uint8_t text_alignment_for_info = 230;
    cmd_text(text_origin_x, text_origin_y, 23, 0,  "Dim Level...................");
    cmd_text(text_origin_x, text_origin_y+text_spacing, 23, 0,  "Calculated Power......");
    text_spacing += 35;
    cmd_text(text_origin_x, text_origin_y+text_spacing, 23, 0,  "Schedule....................");
    text_spacing += 35;
    cmd_text(text_origin_x, text_origin_y+text_spacing, 23, 0, "Dim Style...................");
    text_spacing += 35;
    cmd_text(text_origin_x, text_origin_y+text_spacing, 23, 0, "Dim Correction..........");
    	cmd(TAG_MASK(1));

    FT_SetColorRGB(COLOR_WHITE);   
    text_spacing = 35;
    if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL)
    {
      FT_FGCOLOR(COLOR_GREEN);
    }
    else
    {
      FT_FGCOLOR(COLOR_BLUE);
    }
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL));
    UI_TextHelper_GetDimLevelText(DimmerChannelInfo[channel].dimLevelPercent, display_text_buffer);
    cmd_button(text_alignment_for_info, text_origin_y, 100, 20, 23, 0,  display_text_buffer);  
    
    if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_POWER)
    {
      FT_FGCOLOR(COLOR_GREEN);
    }
    else
    {
      FT_FGCOLOR(COLOR_BLUE);
    }
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_POWER));
    UI_TextHelper_GetCalPowerText(DimmerChannelInfo[channel].calculatedPower, display_text_buffer);
    cmd_button(text_alignment_for_info, text_origin_y+text_spacing, 100, 20, 23, 0,  display_text_buffer);  
    text_spacing += 35;

    if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_SCHEDULE)
    {
      FT_FGCOLOR(COLOR_GREEN);
    }
    else
    {
      FT_FGCOLOR(COLOR_BLUE);
    }    
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_SCHEDULE));
    UI_TextHelper_GetScheduleText(DimmerChannelInfo[channel].schedule, display_text_buffer);
    cmd_button(text_alignment_for_info, text_origin_y+text_spacing, 100, 20, 23, 0,  display_text_buffer);  
    text_spacing += 35;

    if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_STYLE)
    {
      FT_FGCOLOR(COLOR_GREEN);
    }
    else
    {
      FT_FGCOLOR(COLOR_BLUE);
    } 
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_DIM_STYLE));
    UI_TextHelper_GetStyleText(DimmerChannelInfo[channel].dimStyle, display_text_buffer);
    cmd_button(text_alignment_for_info, text_origin_y+text_spacing, 100, 20, 23, 0,  display_text_buffer);  
    text_spacing += 35;

    if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION)
    {
      FT_FGCOLOR(COLOR_GREEN);
    }
    else
    {
      FT_FGCOLOR(COLOR_BLUE);
    } 
    
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION));
    UI_TextHelper_GetCorrectionText(DimmerChannelInfo[channel].dimCorrectionPercent, display_text_buffer);
    cmd_button(text_alignment_for_info, text_origin_y+text_spacing, 100, 20, 23, 0,  display_text_buffer);  
    text_spacing += 35;

    FT_FGCOLOR(COLOR_BLUE);
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_HOME));
    cmd_button(30, 220, 200, 50, 27,0, "HOME");
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_SCHEDULE));
    cmd_button(260, 220, 200, 50, 27,0, "SCHEDULE");    
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_PLUS));
    cmd_button(380, 30, 60, 70, 25,0, "+");
    cmd(TAG(BUTTON_TAG_CHANNEL_MENU_MINUS));
    cmd_button(380, 130, 60, 70, 25,0, "-");
    
    cmd(TAG(UNUSED_TAG_ID));

  FT_SetColorRGB(COLOR_WHITE);
  cmd(BITMAP_SOURCE(RAM_G));
  cmd(BITMAP_LAYOUT(RGB565 , SPLASH_SCREEN_IMAGE_COW_WIDTH*2, SPLASH_SCREEN_IMAGE_COW_HEIGHT));
  cmd(BITMAP_SIZE(NEAREST, BORDER, BORDER, 50, 50)); 
  cmd( BITMAP_TRANSFORM_A(1100) );
  cmd( BITMAP_TRANSFORM_E(1100) );

  cmd(BEGIN(BITMAPS));
  cmd(VERTEX2II(290,8,6,0));
  cmd(END());
    
  cmd(DISPLAY());
	cmd(CMD_SWAP);	    
   

}
#define MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP 5
enum_tag_id touchedTagId;
enum_tag_id LastTouchedChannelMenuTagId;
uint8_t NumConsecutivePlusOrMinusButtonTouch = 0;
channel_menu_action_t ChannelMenuAction = CHANNEL_MENU_NO_ACTION;

void UI_UpdateChannelMenu(void)
{
    touch_info_t touchInfo = Touch_GetTouchXY();
        touchedTagId = Touch_GetTouchTagId();

        //TODO_NOTE_CLEANUP check xy coordinate of touch to get the plus button touch to work correctly instead of using tagid returned by the ft800 ic
      if (touchInfo.point.x >= 380 && touchInfo.point.y >= 30 && touchInfo.point.x <= 440 && touchInfo.point.y <= 100)
      {
        touchedTagId = BUTTON_TAG_CHANNEL_MENU_PLUS;
      }
    
    
    enum_channel_id newChannel;
 
    if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_HOME)
    {
      UI_DrawNewScreen(SCREEN_ID_MAIN_MENU);
      return;
    }
    
    if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL)
    {
      ChannelMenuActiveButton = BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL;
    }
    else if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_POWER)
    {
      ChannelMenuActiveButton = BUTTON_TAG_CHANNEL_MENU_POWER;
    }
    else if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_DIM_STYLE)
    {
      ChannelMenuActiveButton = BUTTON_TAG_CHANNEL_MENU_DIM_STYLE;
    }
    else if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION)
    {
      ChannelMenuActiveButton = BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION;
    }
    else if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_SCHEDULE)
    {
      ChannelMenuActiveButton = BUTTON_TAG_CHANNEL_MENU_SCHEDULE;
    }
    
    if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_PLUS)
    {
      ChannelMenuAction = CHANNEL_MENU_ACTION_PLUS;
      if (LastTouchedChannelMenuTagId == touchedTagId)
      {
        NumConsecutivePlusOrMinusButtonTouch++;
        if (NumConsecutivePlusOrMinusButtonTouch >= MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP)
        {
          NumConsecutivePlusOrMinusButtonTouch = MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP;
        }
      }
      else
      {
        NumConsecutivePlusOrMinusButtonTouch = 0;
      }
      UI_UpdateDimmerChannelInfo();
    }
    else if (touchedTagId == BUTTON_TAG_CHANNEL_MENU_MINUS)
    {
      ChannelMenuAction = CHANNEL_MENU_ACTION_MINUS;
      if (LastTouchedChannelMenuTagId == touchedTagId)
      {
        NumConsecutivePlusOrMinusButtonTouch++;
        if (NumConsecutivePlusOrMinusButtonTouch >= MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP)
        {
          NumConsecutivePlusOrMinusButtonTouch = MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP;
        }
      }
      else
      {
        NumConsecutivePlusOrMinusButtonTouch = 0;
      }
      UI_UpdateDimmerChannelInfo();
    }
    else
    {
      ChannelMenuAction = CHANNEL_MENU_NO_ACTION;
    }
    LastTouchedChannelMenuTagId = touchedTagId;
    UI_DrawChannelMenu();
}

void UI_UpdateScreen(void)
{
  switch(ActiveScreenId)
  {
    case SCREEN_ID_SPLASH:
      //UI_DrawSplashScreen();
      break;
    case SCREEN_ID_MAIN_MENU:
      UI_UpdateMainScreen();
      break;
    case SCREEN_ID_CHANNEL_MENU:
      UI_UpdateChannelMenu();
      break;
    default:
      break;
  }
}

void UI_UpdateDimmerChannelInfo()
{
  float_t dimmerDeltaPercent;
  uint16_t dimmerDeltaPowerStep;
  
  dimmer_channel_t dimmerChannelInfo;
  memcpy(&dimmerChannelInfo, &DimmerChannelInfo[ChannelMenuActiveChannel], sizeof(dimmer_channel_t));

  if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_LEVEL)
  {
    if (NumConsecutivePlusOrMinusButtonTouch >= MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP)
    {
      dimmerDeltaPercent = DIMMING_UI_FAST_CONTROL_STEP;
    }
    else
    {
      dimmerDeltaPercent = DIMMING_UI_NORMAL_CONTROL_STEP;
    }
    if (ChannelMenuAction == CHANNEL_MENU_ACTION_MINUS)
    {
      
      dimmerChannelInfo.dimLevelPercent -= dimmerDeltaPercent;
    }
    else if (ChannelMenuAction == CHANNEL_MENU_ACTION_PLUS)
    {
      dimmerChannelInfo.dimLevelPercent += dimmerDeltaPercent;
    }
    if (dimmerChannelInfo.dimLevelPercent < 0.0)
    {
      dimmerChannelInfo.dimLevelPercent = 0.0;
    }
    else if (dimmerChannelInfo.dimLevelPercent > 100.0)
    {
      dimmerChannelInfo.dimLevelPercent = 100.0;
    }  
  }
  else if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_POWER)
  {
    if (NumConsecutivePlusOrMinusButtonTouch >= MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP)
    {
      dimmerDeltaPowerStep = DIMMING_UI_FAST_POWER_STEP;
    }
    else
    {
      dimmerDeltaPowerStep = DIMMING_UI_NORMAL_POWER_STEP;
    }
    if (ChannelMenuAction == CHANNEL_MENU_ACTION_MINUS)
    {
      if (dimmerChannelInfo.fullOnPower <= 0)
      {
        dimmerChannelInfo.fullOnPower = 0;
      }
      else
      {
        dimmerChannelInfo.fullOnPower -= dimmerDeltaPowerStep;
      }
    }
    else if (ChannelMenuAction == CHANNEL_MENU_ACTION_PLUS)
    {
      dimmerChannelInfo.fullOnPower += dimmerDeltaPowerStep;
    }
    if (dimmerChannelInfo.fullOnPower > 1500)
    {
      dimmerChannelInfo.fullOnPower = 1500;
    }  
    dimmerChannelInfo.calculatedPower = (uint16_t)((float_t)(dimmerChannelInfo.fullOnPower)*dimmerChannelInfo.dimLevelPercent/100.0);
  }
  else if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_STYLE)
  {
    if (ChannelMenuAction == CHANNEL_MENU_ACTION_MINUS)
    {
      dimmerChannelInfo.dimStyle += 1;
    }
    else if (ChannelMenuAction == CHANNEL_MENU_ACTION_PLUS)
    {
      if (dimmerChannelInfo.dimStyle <= 0)
      {
        dimmerChannelInfo.dimStyle = 0;
      }
      else 
      {
        dimmerChannelInfo.dimStyle -= 1;
      }

    }
    if (dimmerChannelInfo.dimStyle >= MAX_NUM_DIM_STYLE)
    {
      dimmerChannelInfo.dimStyle = MAX_NUM_DIM_STYLE;
    }
  }
  else if (ChannelMenuActiveButton == BUTTON_TAG_CHANNEL_MENU_DIM_CORRECTION)
  {
    if (NumConsecutivePlusOrMinusButtonTouch >= MAX_NUM_CONSECUTIVE_PLUS_OR_MINUS_TOUCH_TO_SPEEDUP)
    {
      dimmerDeltaPercent = DIMMING_UI_FAST_CONTROL_STEP;
    }
    else
    {
      dimmerDeltaPercent = .5;
    }
    if (ChannelMenuAction == CHANNEL_MENU_ACTION_MINUS)
    {
      if (dimmerChannelInfo.dimCorrectionPercent < 1.0)
      {
        dimmerChannelInfo.dimCorrectionPercent = 0.0;
      }
      else 
      {
        dimmerChannelInfo.dimCorrectionPercent -= dimmerDeltaPercent;
      }
    }
    else if (ChannelMenuAction == CHANNEL_MENU_ACTION_PLUS)
    {
      dimmerChannelInfo.dimCorrectionPercent += .5;

    }
    
    if (dimmerChannelInfo.dimCorrectionPercent > 10.0)
    {
      dimmerChannelInfo.dimCorrectionPercent = 10.0;
    }  
  }
  DimmerControl_SetChannelInfo(ChannelMenuActiveChannel, &dimmerChannelInfo);

}