#include "stm32f3xx_hal.h"
#include "spi_interface.h"
#include "ft800.h"
#include "gpio_interface.h"
#include "ui_interface.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

uint32_t ft_reg32;
/*
    Function: HOST_MEM_READ_STR
    ARGS:     addr: 24 Bit Command Address 
              pnt:  output buffer for read data
              len:  length of bytes to be read

    Description: Reads len(n) bytes of data, starting at addr into pnt(buffer)
*/
void HOST_MEM_READ_STR(uint32_t addr, uint8_t *pnt, uint16_t len)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(((addr>>16)&0x3F) );			// Send out bits 23:16 of addr, bits 7:6 of this byte must be 00 
  SPI_SendChar(((addr>>8)&0xFF));       	// Send out bits 15:8 of addr
  SPI_SendChar((addr&0xFF));            	// Send out bits 7:0 of addr

  SPI_SendChar(0);                      	// Send out DUMMY (0) byte

  while(len--)                      	// While Len > 0 Read out n bytes
    *pnt++ = SPI_SendChar(0);
  
  IO_SetPin(IO_PIN_SPI_CS);
}

/*
    Function: HOST_MEM_WR_STR
    ARGS:     addr: 24 Bit Command Address 
              pnt:  input buffer of data to send
              len:  length of bytes to be send

    Description: Writes len(n) bytes of data from pnt (buffer) to addr
*/
void HOST_MEM_WR_STR(uint32_t addr, uint8_t *pnt, uint16_t len)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(((addr>>16)&0x3F)|0x80);     // Send out 23:16 of addr, bits 7:6 of this byte must be 10
  SPI_SendChar(((addr>>8)&0xFF));           // Send out bits 15:8 of addr
  SPI_SendChar((addr&0xFF));                // Send out bits 7:0 of addr

  while(len--)                          // While Len > 0 Write *pnt (then increment pnt)
    SPI_SendChar(*pnt++);
  
  IO_SetPin(IO_PIN_SPI_CS);
}

/*
    Function: HOST_CMD_WRITE
    ARGS:     CMD:  5 bit Command
             
    Description: Writes Command to FT800
*/
void HOST_CMD_WRITE(uint8_t CMD)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar((uint8_t)(CMD|0x40));        // Send out Command, bits 7:6 must be 01
  SPI_SendChar(0x00);
  SPI_SendChar(0x00);
  IO_SetPin(IO_PIN_SPI_CS);
}

void HOST_CMD_ACTIVE(void)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(0x00);      
  SPI_SendChar(0x00);
  SPI_SendChar(0x00);
  IO_SetPin(IO_PIN_SPI_CS);
}

/*
    Function: HOST_MEM_WR8
    ARGS:     addr: 24 Bit Command Address 
              data: 8bit Data Byte

    Description: Writes 1 byte of data to addr
*/
void HOST_MEM_WR8(uint32_t addr, uint8_t data)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar((addr>>16)|0x80);
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));

  SPI_SendChar(data);
  
  IO_SetPin(IO_PIN_SPI_CS);  
}

/*
    Function: HOST_MEM_WR16
    ARGS:     addr: 24 Bit Command Address 
              data: 16bit (2 bytes)

    Description: Writes 2 bytes of data to addr
*/
void HOST_MEM_WR16(uint32_t addr, uint32_t data)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar((addr>>16)|0x80);
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));

  /* Little-Endian: Least Significant Byte to: smallest address */
  SPI_SendChar( (uint8_t)((data&0xFF)) );    //byte 0
  SPI_SendChar( (uint8_t)((data>>8)) );      //byte 1
  
  IO_SetPin(IO_PIN_SPI_CS);  
}

/*
    Function: HOST_MEM_WR32
    ARGS:     addr: 24 Bit Command Address 
              data: 32bit (4 bytes)

    Description: Writes 4 bytes of data to addr
*/
void HOST_MEM_WR32(uint32_t addr, uint32_t data)
{
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar((addr>>16)|0x80);
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));

  SPI_SendChar( (uint8_t)(data&0xFF) );
  SPI_SendChar( (uint8_t)((data>>8)&0xFF) );
  SPI_SendChar( (uint8_t)((data>>16)&0xFF) );
  SPI_SendChar( (uint8_t)((data>>24)&0xFF) );
  
  IO_SetPin(IO_PIN_SPI_CS);  
}

/*
    Function: HOST_MEM_RD8
    ARGS:     addr: 24 Bit Command Address 

    Description: Returns 1 byte of data from addr
*/
uint8_t HOST_MEM_RD8(uint32_t addr)
{
  uint8_t data_in;

  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar((uint8_t)((addr>>16)&0x3F));
  SPI_SendChar((uint8_t)((addr>>8)&0xFF));
  SPI_SendChar((uint8_t)(addr));
  SPI_SendChar(0);

  data_in = SPI_SendChar(0);
  
  IO_SetPin(IO_PIN_SPI_CS);
  return data_in;
}

/*
    Function: HOST_MEM_RD16
    ARGS:     addr: 24 Bit Command Address 

    Description: Returns 2 byte of data from addr in a 32bit variable
*/
uint32_t HOST_MEM_RD16(uint32_t addr)
{
  uint8_t data_in = 0;
  uint32_t data = 0;
  uint8_t i;

  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(((addr>>16)&0x3F));
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));
  SPI_SendChar(0);

  for(i=0;i<2;i++)
  {
    data_in = SPI_SendChar(0);
    data |= ( ((uint32_t)data_in) << (8*i) );
  }
  
  IO_SetPin(IO_PIN_SPI_CS);
  return data;
}

/*
    Function: HOST_MEM_RD32
    ARGS:     addr: 24 Bit Command Address 

    Description: Returns 4 byte of data from addr in a 32bit variable
*/
uint32_t HOST_MEM_RD32(uint32_t addr)
{
  uint8_t data_in = 0;
  uint32_t data = 0;
  uint8_t i;

  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(((addr>>16)&0x3F));
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));
  SPI_SendChar(0);

  for(i=0;i<4;i++)
  {
    data_in = SPI_SendChar(0);
    data |= ( ((uint32_t)data_in) << (8*i) );
  }
  ft_reg32 = data;
  IO_SetPin(IO_PIN_SPI_CS);
  return data;
}

/*** CMD Functions *****************************************************************/
uint8_t cmd_execute(uint32_t data)
{
	uint32_t cmdBufferRd = 0;
  uint32_t cmdBufferWr = 0;
  
  cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
  
  uint32_t cmdBufferDiff = cmdBufferWr-cmdBufferRd;
  
  if( (4096-cmdBufferDiff) > 4)
  {      
      HOST_MEM_WR32(RAM_CMD + cmdBufferWr, data);
      HOST_MEM_WR32(REG_CMD_WRITE, cmdBufferWr + 4);
      
      return 1;
  }
  return 0;
}

uint8_t cmd(uint32_t data)
{
	uint8_t tryCount = 255;
	for(tryCount = 255; tryCount > 0; --tryCount)
	{
		if(cmd_execute(data)) { return 1; }
	}
	return 0;
}

uint8_t cmd_ready(void)
{
    uint32_t cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
    uint32_t cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
    
    return (cmdBufferRd == cmdBufferWr) ? 1 : 0;
}

/*** Track *************************************************************************/
void cmd_track(int16_t x, int16_t y, int16_t w, int16_t h, int16_t tag)
{
    cmd(CMD_TRACK);
    cmd( ((uint32_t)y<<16)|(x & 0xffff) );
    cmd( ((uint32_t)h<<16)|(w & 0xffff) );
    cmd( (uint32_t)tag );
}

/*** Draw Spinner ******************************************************************/
void cmd_spinner(int16_t x, int16_t y, uint16_t style, uint16_t scale)
{    
    cmd(CMD_SPINNER);
    cmd( ((uint32_t)y<<16)|(x & 0xffff) );
    cmd( ((uint32_t)scale<<16)|style );
    
}

/*** Draw Slider *******************************************************************/
void cmd_slider(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t options, uint16_t val, uint16_t range)
{
	cmd(CMD_SLIDER);
	cmd( ((uint32_t)y<<16)|(x & 0xffff) );
	cmd( ((uint32_t)h<<16)|(w & 0xffff) );
	cmd( ((uint32_t)val<<16)|(options & 0xffff) );
	cmd( (uint32_t)range );
}

/*** Draw Text *********************************************************************/
void cmd_text(int16_t x, int16_t y, int16_t font, uint16_t options, const char* str)
{
	/* 	
		i: data pointer
		q: str  pointer
		j: loop counter
	*/
	
	uint16_t i,j,q;
	const uint16_t length = strlen(str);
	if(!length) return ;	
	
	uint32_t* data = (uint32_t*) calloc((length/4)+1, sizeof(uint32_t));
	
	q = 0;
	for(i=0; i<(length/4); ++i, q=q+4)
	{
		data[i] = (uint32_t)str[q+3]<<24 | (uint32_t)str[q+2]<<16 | (uint32_t)str[q+1]<<8 | (uint32_t)str[q];
	}
	for(j=0; j<(length%4); ++j, ++q)
	{
		data[i] |= (uint32_t)str[q] << (j*8);
	}
	
	cmd(CMD_TEXT);
	cmd( ((uint32_t)y<<16)|(x & 0xffff) );
    cmd( ((uint32_t)options<<16)|(font & 0xffff) );
	for(j=0; j<(length/4)+1; ++j)
	{
		cmd(data[j]);
	}
	free(data);
}

/*** Draw Button *******************************************************************/
void cmd_button(int16_t x, int16_t y, int16_t w, int16_t h, int16_t font, uint16_t options, const char* str)
{	
	/* 	
		i: data pointer
		q: str  pointer
		j: loop counter
	*/
	
	uint16_t i,j,q;
	const uint16_t length = strlen(str);
	if(!length) return ;
	
	uint32_t* data = (uint32_t*) calloc((length/4)+1, sizeof(uint32_t));
	
	q = 0;
	for(i=0; i<(length/4); ++i, q=q+4)
	{
		data[i] = (uint32_t)str[q+3]<<24 | (uint32_t)str[q+2]<<16 | (uint32_t)str[q+1]<<8 | (uint32_t)str[q];
	}
	for(j=0; j<(length%4); ++j, ++q)
	{
		data[i] |= (uint32_t)str[q] << (j*8);
	}
	
	cmd(CMD_BUTTON);
	cmd( ((uint32_t)y<<16)|(x & 0xffff) );
	cmd( ((uint32_t)h<<16)|(w & 0xffff) );
    cmd( ((uint32_t)options<<16)|(font & 0xffff) );
	for(j=0; j<(length/4)+1; ++j)
	{
		cmd(data[j]);
	}
	free(data);
}

/*** Draw Keyboard *****************************************************************/
void cmd_keys(int16_t x, int16_t y, int16_t w, int16_t h, int16_t font, uint16_t options, const char* str)
{
	/* 	
		i: data pointer
		q: str  pointer
		j: loop counter
	*/
	
	uint16_t i,j,q;
	const uint16_t length = strlen(str);
	if(!length) return ;
	
	uint32_t* data = (uint32_t*) calloc((length/4)+1, sizeof(uint32_t));
	
	q = 0;
	for(i=0; i<(length/4); ++i, q=q+4)
	{
		data[i] = (uint32_t)str[q+3]<<24 | (uint32_t)str[q+2]<<16 | (uint32_t)str[q+1]<<8 | (uint32_t)str[q];
	}
	for(j=0; j<(length%4); ++j, ++q)
	{
		data[i] |= (uint32_t)str[q] << (j*8);
	}
	
	cmd(CMD_KEYS);
	cmd( ((uint32_t)y<<16)|(x & 0xffff) );
	cmd( ((uint32_t)h<<16)|(w & 0xffff) );
    cmd( ((uint32_t)options<<16)|(font & 0xffff) );
	for(j=0; j<(length/4)+1; ++j)
	{
		cmd(data[j]);
	}
	free(data);
}

/*** Write zero to a block of memory ***********************************************/
void cmd_memzero(uint32_t ptr, uint32_t num)
{
	cmd(CMD_MEMZERO);
	cmd(ptr);
	cmd(num);
}

/*** Set FG color ******************************************************************/
void cmd_fgcolor(uint32_t c)
{
	cmd(CMD_FGCOLOR);
	cmd(c);
}

/*** Set BG color ******************************************************************/
void cmd_bgcolor(uint32_t c)
{
	cmd(CMD_BGCOLOR);
	cmd(c);
}

/*** Set Gradient color ************************************************************/
void cmd_gradcolor(uint32_t c)
{
	cmd(CMD_GRADCOLOR);
	cmd(c);
}

/*** Draw Gradient *****************************************************************/
void cmd_gradient(int16_t x0, int16_t y0, uint32_t rgb0, int16_t x1, int16_t y1, uint32_t rgb1)
{
	cmd(CMD_GRADIENT);
	cmd( ((uint32_t)y0<<16)|(x0 & 0xffff) );
	cmd(rgb0);
	cmd( ((uint32_t)y1<<16)|(x1 & 0xffff) );
	cmd(rgb1);
}

/*** Matrix Functions **************************************************************/
void cmd_loadidentity(void)
{
	cmd(CMD_LOADIDENTITY);
}

void cmd_setmatrix(void)
{
	cmd(CMD_SETMATRIX);
}

void cmd_rotate(int32_t angle)
{
	cmd(CMD_ROTATE);
	cmd(angle);
}

void cmd_translate(int32_t tx, int32_t ty)
{
	cmd(CMD_TRANSLATE);
	cmd(tx);
	cmd(ty);
}

uint8_t initFT800(void)
{   
	uint8_t dev_id = 0;                  // Variable for holding the read device id    
	IO_ClearPin(IO_PIN_FT_PD);   // Set the PDN pin low 

	HAL_Delay(100);                          // Delay 50 ms for stability
	IO_SetPin(IO_PIN_FT_PD); 	 // Set the PDN pin high
	HAL_Delay(50);                          // Delay 50 ms for stability

	//WAKE
	HOST_CMD_ACTIVE();
	HAL_Delay(500);

	//Ext Clock
	HOST_CMD_WRITE(CMD_CLKINT);         // Send CMD_CLKINT Command (0x44)

	//PLL (48M) Clock
	HOST_CMD_WRITE(CMD_CLK48M);         // Send CLK_48M Command (0x62)

	//Read Dev ID
	dev_id = HOST_MEM_RD8(REG_ID);      // Read device id
	if(dev_id != 0x7C)                  // Device ID should always be 0x7C
	{   
		return 1;
	}

	HOST_MEM_WR8(REG_GPIO, 0x00);			// Set REG_GPIO to 0 to turn off the LCD DISP signal
	HOST_MEM_WR8(REG_PCLK, 0x00);      		// Pixel Clock Output disable

	HOST_MEM_WR16(REG_HCYCLE, 548);         // Set H_Cycle to 548
	HOST_MEM_WR16(REG_HOFFSET, 43);         // Set H_Offset to 43
	HOST_MEM_WR16(REG_HSYNC0, 0);           // Set H_SYNC_0 to 0
	HOST_MEM_WR16(REG_HSYNC1, 41);          // Set H_SYNC_1 to 41
	HOST_MEM_WR16(REG_VCYCLE, 292);         // Set V_Cycle to 292
	HOST_MEM_WR16(REG_VOFFSET, 12);         // Set V_OFFSET to 12
	HOST_MEM_WR16(REG_VSYNC0, 0);           // Set V_SYNC_0 to 0
	HOST_MEM_WR16(REG_VSYNC1, 10);          // Set V_SYNC_1 to 10
	HOST_MEM_WR8(REG_SWIZZLE, 0);           // Set SWIZZLE to 0
	HOST_MEM_WR8(REG_PCLK_POL, 1);          // Set PCLK_POL to 1
	HOST_MEM_WR8(REG_CSPREAD, 1);           // Set CSPREAD to 1
	HOST_MEM_WR16(REG_HSIZE, 480);          // Set H_SIZE to 480
	HOST_MEM_WR16(REG_VSIZE, 272);          // Set V_SIZE to 272

	/* configure touch & audio */
	HOST_MEM_WR8(REG_TOUCH_MODE, 0x03);     	//set touch on: continous
	HOST_MEM_WR8(REG_TOUCH_ADC_MODE, 0x01); 	//set touch mode: differential
	HOST_MEM_WR8(REG_TOUCH_OVERSAMPLE, 0x0F); 	//set touch oversampling to max
	HOST_MEM_WR16(REG_TOUCH_RZTHRESH, 2000);	//set touch resistance threshold
	HOST_MEM_WR8(REG_VOL_SOUND, 0xFF);      	//set the volume to maximum

	/* write first display list */
	HOST_MEM_WR32(RAM_DL+0, CLEAR_COLOR_RGB(255,255,255));  // Set Initial Color to BLACK
	HOST_MEM_WR32(RAM_DL+4, CLEAR(1,1,1));            // Clear to the Initial Color
	HOST_MEM_WR32(RAM_DL+8, DISPLAY());               // End Display List

	HOST_MEM_WR8(REG_DLSWAP, DLSWAP_FRAME);           // Make this display list active on the next frame

	HOST_MEM_WR8(REG_GPIO_DIR, 0x80);                 // Set Disp GPIO Direction 
	HOST_MEM_WR8(REG_GPIO, 0x80);                     // Enable Disp (if used)
	HOST_MEM_WR16(REG_PWM_HZ, 0x00FA);                // Backlight PWM frequency
	HOST_MEM_WR8(REG_PWM_DUTY, 80);                 // Backlight PWM duty    

	HOST_MEM_WR8(REG_PCLK, 0x05);                     // After this display is visible on the LCD

	return 0;
}

void cmd_calibrate()
{
	cmd(CMD_DLSTART);
	FT_CLEAR_RGB(DEFAULT_BACKGROUND);
	FT_CLEAR_BUFFER();
    
	FT_SET_COLOR(COLOR_BLACK);
	
    cmd(CMD_CALIBRATE);
    cmd(0);

	cmd(DISPLAY());
	cmd(CMD_SWAP);	    
}

void FT_WriteRawJpegToRam(uint32_t addr, const uint8_t *data, uint32_t dataLength)
{
  assert((dataLength + addr) < FT_RAM_G_SIZE);
  IO_ClearPin(IO_PIN_SPI_CS);
  SPI_SendChar(((addr>>16)&0x3F)|0x80);     // Send out 23:16 of addr, bits 7:6 of this byte must be 10
  SPI_SendChar(((addr>>8)&0xFF));           // Send out bits 15:8 of addr
  SPI_SendChar((addr&0xFF));                // Send out bits 7:0 of addr

  while(dataLength--)                          // While Len > 0 Write *pnt (then increment pnt)
    SPI_SendChar(*data++);
  
  IO_SetPin(IO_PIN_SPI_CS);
  
}

void FT_InflateBinJpegToRam(uint32_t addr, const uint8_t *data, uint32_t dataLength)
{
  uint32_t buff;
	uint32_t cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  uint32_t cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
  //FT_WriteCmdToFifoBuff(CMD_INFLATE, inflateDest, 4, data, dataLength);
  IO_ClearPin(IO_PIN_SPI_CS);
  buff = RAM_CMD + cmdBufferWr;
  
  SPI_SendChar((buff>>16)|0x80);
  SPI_SendChar(((buff>>8)&0xFF));
  SPI_SendChar((buff&0xFF));

  buff = CMD_INFLATE;
  SPI_SendChar( (uint8_t)(buff&0xFF) );
  SPI_SendChar( (uint8_t)((buff>>8)&0xFF) );
  SPI_SendChar( (uint8_t)((buff>>16)&0xFF) );
  SPI_SendChar( (uint8_t)((buff>>24)&0xFF) );
  cmdBufferWr+=4;
  SPI_SendChar( (uint8_t)(addr&0xFF) );
  SPI_SendChar( (uint8_t)((addr>>8)&0xFF) );
  SPI_SendChar( (uint8_t)((addr>>16)&0xFF) );
  SPI_SendChar( (uint8_t)((addr>>24)&0xFF) );
  cmdBufferWr+=4;
  uint32_t count=0;
  uint8_t temp[128]={0}
  ;for (uint32_t i = 0; i < dataLength; i++)
  {
    if (count<FT_CMD_FIFO_SIZE-4)
    {
      count++;
      SPI_SendChar(data[i]);
      cmdBufferWr++;
    }
    else
    {
      IO_SetPin(IO_PIN_SPI_CS);
      HOST_MEM_WR32(REG_CMD_WRITE, cmdBufferWr + 4);
      cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
//      cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
      FT_WaitFifoEmpty(10);
     
      for (uint8_t i=0; i<128; i++)
      {
        temp[i] = HOST_MEM_RD8(i);
      }
      
      IO_ClearPin(IO_PIN_SPI_CS);
      buff = RAM_CMD + cmdBufferWr;
      
 //     SPI_SendChar((buff>>16)|0x80);
 //     SPI_SendChar(((buff>>8)&0xFF));
 //     SPI_SendChar((buff&0xFF));
 

      SPI_SendChar(data[i]);
      cmdBufferWr++;
      count=0;

    }
  }

  uint8_t length = (4-(dataLength%4))%4;
  for (uint8_t i=0; i < length; i++)
  {
    SPI_SendChar(0);
    cmdBufferWr++;
  }

    IO_SetPin(IO_PIN_SPI_CS);

	//cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  //cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
  HOST_MEM_WR32(REG_CMD_WRITE, cmdBufferWr + 4);
  cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
	cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  FT_WaitFifoEmpty(10);
  cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
	cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  cmd(CMD_GETPTR);
	cmdBufferRd = HOST_MEM_RD32(RAM_CMD + cmdBufferRd +4);
      for (uint8_t i=0; i<128; i++)
      {
        temp[i] = HOST_MEM_RD8(i);
      }
  
}
                      
uint8_t FT_WaitFifoEmpty(uint32_t timeOutInMs)
{
	uint32_t cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
  uint32_t cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
  uint32_t startTimeInMs = HAL_GetTick();
    
  while (cmdBufferRd != cmdBufferWr)
  {
    cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);
    cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);    
    if (HAL_GetTick() - startTimeInMs > timeOutInMs)
    {
      return -1;
    }
  }  
  return 0;
}

void FT_WriteCmdToFifoBuff(uint32_t cmd, uint8_t *params, uint8_t paramLength, const uint8_t *data, uint32_t dataLength)
{
	uint32_t cmdBufferRd = 0;
  uint32_t cmdBufferWr = 0;
  uint32_t count;
  uint32_t addr;
  
  FT_WaitFifoEmpty(10);
    
  cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);

  IO_ClearPin(IO_PIN_SPI_CS);
  addr = RAM_CMD + cmdBufferWr;
  SPI_SendChar((addr>>16)|0x80);
  SPI_SendChar(((addr>>8)&0xFF));
  SPI_SendChar((addr&0xFF));  
  for (uint8_t i=0; i<paramLength; i++)
  {
    SPI_SendChar(params[i]);
  }
  IO_SetPin(IO_PIN_SPI_CS);
      HOST_MEM_WR32(REG_CMD_WRITE, cmdBufferWr + 3 + paramLength + 4);
      cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
      cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);  
      FT_WaitFifoEmpty(10);
  IO_ClearPin(IO_PIN_SPI_CS);
  for (uint32_t i=0; i<dataLength; i++)
  {
      cmdBufferWr = HOST_MEM_RD32(REG_CMD_WRITE);
      cmdBufferRd = HOST_MEM_RD32(REG_CMD_READ);    
    if (i>=FT_RAM_G_SIZE)
    {
      IO_SetPin(IO_PIN_SPI_CS);
      HOST_MEM_WR32(REG_CMD_WRITE, cmdBufferWr + 4);
      FT_WaitFifoEmpty(10);
      IO_ClearPin(IO_PIN_SPI_CS);
    }
    else
    {
      SPI_SendChar(data[i]);
    }
  }

}

void FT_SetColorRGB(uint32_t rgbColor)
{
  uint8_t red = (rgbColor>>16)&0xff;
  uint8_t green = (rgbColor>>8)&0xff;
  uint8_t blue = (rgbColor)&0xff;
  
  cmd( COLOR_RGB(red, green, blue));  
}

void FT_DrawRectangle(ft_rectangle_t rect)
{
  FT_SetColorRGB(rect.borderColor);
  cmd( LINE_WIDTH(rect.cornerCurvature * 16) );
  cmd( BEGIN(RECTS) );
  cmd( VERTEX2F(rect.corner1.x * 16, rect.corner1.y* 16) );
  cmd( VERTEX2F(rect.corner2.x * 16, rect.corner2.y * 16) );
  FT_SetColorRGB(rect.innerColor);
  cmd( VERTEX2F((rect.corner1.x + rect.borderWidth)* 16, (rect.corner1.y + rect.borderWidth)* 16) );
  cmd( VERTEX2F((rect.corner2.x - rect.borderWidth)* 16, (rect.corner2.y - rect.borderWidth)* 16) );
  cmd(END());  
}