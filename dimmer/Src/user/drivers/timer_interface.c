#include "timer_interface.h"
#include "adc_interface.h"

#define TIMER_BASE_CLK_FREQ ((uint32_t)48000000)

void Timer_Start()
{
  HAL_TIM_Base_Start_IT(&htim1);
}

uint32_t Timer_GetPrescalerFromFreq(TIM_TypeDef *timer, uint32_t freq)
{
  uint32_t prescaler;
  prescaler = (TIMER_BASE_CLK_FREQ / freq) - 1U;
  return prescaler;
}

void Timer_InitOnePulseChannel(enum_channel_id channel)
{
  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OnePulse_InitTypeDef sConfig;
  TIM_HandleTypeDef *htim;
  TIM_TypeDef *timer;
  
  if (DimmerChannelInfo[channel].isPresent == true)
  {
    htim = DimmerChannelInfo[channel].htim;
    timer = DimmerChannelInfo[channel].timer;

    htim->Instance = timer;
    htim->Init.Prescaler = Timer_GetPrescalerFromFreq(timer, DESIRED_TIMER_FREQ);
    htim->Init.CounterMode = TIM_COUNTERMODE_UP;
    htim->Init.Period = 0;
    htim->Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim->Init.RepetitionCounter = 0;
    htim->Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(htim) != HAL_OK)
    {
      Error_Handler();
    }
      sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(htim, &sClockSourceConfig) != HAL_OK)
    {
      Error_Handler();
    }

    if (HAL_TIM_OnePulse_Init(htim, TIM_OPMODE_SINGLE) != HAL_OK)
    {
      Error_Handler();
    }
      sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(htim, &sMasterConfig) != HAL_OK)
    {
      Error_Handler();
    }
    
    sConfig.OCMode       = TIM_OCMODE_PWM2;
    sConfig.OCPolarity   = TIM_OCPOLARITY_HIGH;
    sConfig.Pulse        = 1000;
    sConfig.ICPolarity   = TIM_ICPOLARITY_FALLING;
    sConfig.ICSelection  = TIM_ICSELECTION_DIRECTTI;
    sConfig.ICFilter     = 0;
    sConfig.OCNPolarity  = TIM_OCNPOLARITY_HIGH;
    sConfig.OCIdleState  = TIM_OCIDLESTATE_SET;
    sConfig.OCNIdleState = TIM_OCNIDLESTATE_RESET;

    if (HAL_TIM_OnePulse_ConfigChannel(htim, &sConfig, TIM_CHANNEL_2, TIM_CHANNEL_1) != HAL_OK)
    {
      /* Configuration Error */
      Error_Handler();
    }
    if (HAL_TIM_OnePulse_Start(htim, TIM_CHANNEL_2) != HAL_OK)
    {
      /* Starting Error */
      Error_Handler();
    }
    HAL_TIM_MspPostInit(htim);
  }
}

void Timer_SetOnePulseDelayMicroseconds(enum_channel_id channel, uint16_t microSecondDelay)
{
  TIM_HandleTypeDef *htim = DimmerChannelInfo[channel].htim;
  uint32_t timChannel = TIM_CHANNEL_2;

  uint32_t registerCount = microSecondDelay * (DESIRED_TIMER_FREQ/ONE_MEGA_HZ);
  __HAL_TIM_SET_COMPARE(htim, timChannel, registerCount);
}

void Timer_SetOnePulseActiveMicroSeconds(enum_channel_id channel, uint16_t activeTimeMicrosecond)
{
  TIM_HandleTypeDef *htim = DimmerChannelInfo[channel].htim;
  uint32_t timChannel = TIM_CHANNEL_2;
  uint32_t registerCount;

  registerCount = activeTimeMicrosecond * (DESIRED_TIMER_FREQ/ONE_MEGA_HZ) +   __HAL_TIM_GET_COMPARE(htim, timChannel)+1;
  __HAL_TIM_SET_AUTORELOAD(htim, registerCount);  
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  uint32_t timChannel = TIM_CHANNEL_2;

  for (uint8_t channel = CHANNEL_1A; channel < MAX_NUM_DIMMER_CHANNEL; channel++)
  {
    if (htim == DimmerChannelInfo[channel].htim)
    {
      __HAL_TIM_SET_COMPARE(htim, timChannel, MIN_DIMMING_DELAY_START + DimmerChannelInfo[channel].dimCorrectionInTimerTicks);
      __HAL_TIM_SET_AUTORELOAD(htim, DimmerChannelInfo[channel].dimLevelInTimerTicks);  
    }
  }
}

void Timer_DelayMs(uint32_t delayMs)
{
  uint32_t tickStart = HAL_GetTick();
  while (HAL_GetTick() - tickStart < delayMs);
}