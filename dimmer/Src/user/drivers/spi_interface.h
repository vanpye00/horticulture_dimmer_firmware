#ifndef SPI_INTERFACE_H
#define SPI_INTERFACE_H

char SPI_SendChar(char data);
void SPI_SendString(uint8_t *data, uint32_t dataLength);

#endif