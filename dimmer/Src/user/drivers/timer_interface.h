#ifndef TIMER_INTERFACE_H
#define TIMER_INTERFACE_H

#include "tim.h"
#include "dimmer_control.h"

#define delay_ms(__x__) HAL_Delay(__x__)
#define DESIRED_TIMER_FREQ ((uint32_t)4000000)
#define ONE_MEGA_HZ ((uint32_t)1000000)
#define AC_MAIN_PERIOD (8333U*4U)

void Timer_Start(void);
void Timer_DelayMs(uint32_t delayMs);

uint32_t Timer_GetPrescalerFromFreq(TIM_TypeDef *timer, uint32_t freq);
void Timer_InitOnePulseChannel(enum_channel_id channel);
void Timer_SetOnePulseActiveMicroSeconds(enum_channel_id channel, uint16_t microSecondDelay);
void Timer_SetOnePulseDelayMicroseconds(enum_channel_id channel, uint16_t microSecondDelay);

#endif
