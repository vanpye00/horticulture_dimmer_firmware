#include "main_user.h"
#include "adc_interface.h"
#include "adc.h"
#include "math.h"

#define REF_VOLTAGE 3.3
#define MAX_NUM_SAMPLES 50U

uint16_t AnalogBuffer[MAX_ANALOG_BUFFER];
volatile uint8_t ADC_IsConversionDone = 0;
static float RawAnalogValue[MAX_ANALOG_BUFFER];
static float RunningSum[MAX_NUM_ADC_CHANNEL] ={0.0};
static float ChannelValue[MAX_NUM_ADC_CHANNEL];
static uint8_t RunningIndex[MAX_NUM_ADC_CHANNEL] = {0};
static float LastAnalogValues[MAX_NUM_ADC_CHANNEL] ={0.0};
uint16_t ChannelControlPerThousand[MAX_NUM_ADC_CHANNEL] = {0};
uint8_t ADC_HasNewControlValue[MAX_NUM_ADC_CHANNEL] = {0};

void ADC_Start(void)
{
  HAL_ADC_Start_DMA(&hadc4, (uint32_t *)AnalogBuffer, MAX_ANALOG_BUFFER);
}

void ADC_ProcessNewConversions(void)
{
  uint8_t channelIndex;
  float temp;
  float diff;
  
  for (uint8_t i=0; i < MAX_ANALOG_BUFFER; i++)
  {
    temp = ((float) AnalogBuffer[i]) * 3.3 / (float)(1<<12);
    if (i%2 == 0)
    {
      channelIndex = 0;
    }
    else
    {
      channelIndex = 1;
    }
    diff = temp - LastAnalogValues[channelIndex];
    if (diff > .002)
    {
      RawAnalogValue[i] = LastAnalogValues[channelIndex] + .002;
    }
    else if (diff < .002)
    {
      RawAnalogValue[i] = LastAnalogValues[channelIndex] - .002;      
    }
    else
    {
      RawAnalogValue[i] = temp;
    }
    LastAnalogValues[channelIndex] = RawAnalogValue[i];
    RunningSum[channelIndex]+= RawAnalogValue[i];
    RunningIndex[channelIndex]++;
    if (RunningIndex[channelIndex] >= MAX_NUM_SAMPLES)
    {
      ChannelValue[channelIndex] = RunningSum[channelIndex] / MAX_NUM_SAMPLES;
      ChannelControlPerThousand[channelIndex] = (uint16_t)( ChannelValue[channelIndex] *1000.0 / REF_VOLTAGE);
      RunningSum[channelIndex] = 0.0;
      RunningIndex[channelIndex] = 0;
      ADC_HasNewControlValue[channelIndex] = 1;
    }
  }
  
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  ADC_IsConversionDone = true;  
}
