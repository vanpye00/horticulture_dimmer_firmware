#ifndef ADC_INTERFACE_H
#define ADC_INTERFACE_H

#include <stdBool.h>

#define MAX_CHANNEL 2U
#define MAX_ANALOG_BUFFER 16U

typedef enum
{
  CHANNEL_A_0_TO_10,
  CHANNEL_B_0_TO_10,
  MAX_NUM_ADC_CHANNEL,
}adc_channel_t;

extern uint16_t AnalogBuffer[16];
extern volatile uint8_t ADC_IsConversionDone;
extern uint8_t ADC_HasNewControlValue[MAX_CHANNEL];
extern uint16_t ChannelControlPerThousand[MAX_CHANNEL];

void ADC_Start(void);
void ADC_ProcessNewConversions(void);

#endif