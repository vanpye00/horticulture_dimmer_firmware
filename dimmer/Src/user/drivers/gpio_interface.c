#include "gpio_interface.h"

bool Gpio_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
  if (HAL_GPIO_ReadPin(GPIOx, GPIO_Pin) == GPIO_PIN_RESET)
  {
    return 0;
  }
  return 1;
}
