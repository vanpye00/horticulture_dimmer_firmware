#include "stm32f3xx_hal.h"
#include "spi_interface.h"
#include "main_user.h"

#define DEFAULT_SPI_TIMEOUT 5000 //5 seconds

uint8_t rx_buff;

char SPI_SendChar(char data)
{
    HAL_SPI_TransmitReceive(&hspi1 , (uint8_t *) &data, &rx_buff, sizeof(char), DEFAULT_SPI_TIMEOUT);
    return (char) rx_buff;
}

void SPI_SendString(uint8_t *data, uint32_t dataLength)
{
    HAL_SPI_TransmitReceive(&hspi1 , (uint8_t *) &data, &rx_buff, dataLength, DEFAULT_SPI_TIMEOUT);
}

void SPI_SendUint32(uint32_t data)
{
  uint8_t buff[4];
  uint8_t dummy[4];
  buff[0] = data & 0xff;
  buff[1] = (data>>8)&0xff;
  buff[2] = (data>>16)&0xff;
  buff[3] = (data>>24)&0xff;
  HAL_SPI_TransmitReceive(&hspi1 , (uint8_t *) buff, dummy, 4, DEFAULT_SPI_TIMEOUT);
}