#ifndef GPIO_INTERFACE_H
#define GPIO_INTERFACE_H

#include "main.h"
#include "stdbool.h"

#define IO_PIN_SPI_CS LCD_CS_GPIO_Port, LCD_CS_Pin
#define IO_PIN_FT_PD LCD_PDN_GPIO_Port, LCD_PDN_Pin
#define IO_PIN_SENSE1 Sense1_GPIO_Port, Sense1_Pin
#define IO_PIN_SENSE2 Sense2_GPIO_Port, Sense2_Pin

#define IO_SetPin(__pin__) HAL_GPIO_WritePin(__pin__, GPIO_PIN_SET)
#define IO_ClearPin(__pin__) HAL_GPIO_WritePin(__pin__, GPIO_PIN_RESET)
#define IO_ReadPin(__pin__) GpioInterface_ReadPin(__pin__)

bool Gpio_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

#endif