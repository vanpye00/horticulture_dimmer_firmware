#include "main_user.h"
#include "ft800.h"
#include "spi_interface.h"
#include "gpio_interface.h"
#include "ui_interface.h"
#include "timer_interface.h"
#include "adc_interface.h"
#include "touch_interface.h"
#include "dimmer_control.h"

void lcd_start_screen(uint8_t button);
uint32_t tag;
uint32_t tag_id;
uint16_t zcd_point;

int main_user(void)
{ 
  uint32_t lastTimeUpdateMainScreen = HAL_GetTick();
  DimmerControl_Init();
  
	initFT800();
	delay_ms(50);
  UI_DrawNewScreen(SCREEN_ID_SPLASH);
  UI_SetCalibrateParameter();
  UI_DrawNewScreen(SCREEN_ID_MAIN_MENU);
  
  //Timer_Start();
  ADC_Start();  
  Timer_DelayMs(100);
  for (uint8_t i=0; i<MAX_NUM_DIMMER_CHANNEL; i++)
  {
    Timer_InitOnePulseChannel(i);
  }
	while(1)
	{
    if (HAL_GetTick() - lastTimeUpdateMainScreen > 30)
    {
      if(UI_IsScreenReplying())
      {
        initFT800();
        UI_SetCalibrateParameter();
      }
      UI_UpdateScreen();
      lastTimeUpdateMainScreen = HAL_GetTick();
    }

		//tag = ui_get_touched_tag_id();
		//button pressed
		//if(tag != 0)
		{
      //ui_process_new_button_touched(tag);
//      delay_ms(50);
      //tag = ui_get_touched_tag_id();
     // while (tag!=0)
      {
        //tag = ui_get_touched_tag_id(); 
      }      
 		}
    
	}    
    return 0;
}



